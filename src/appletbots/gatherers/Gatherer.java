/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.gatherers;

import appletbots.CarriableObject;
import appletbots.CarrierAgent;
import appletbots.CollisionException;
import appletbots.WorldObject;
import appletbots.geometry.Vector;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A gatherer agent chooses the closest visible piece of food
 * that is currently not being carried by anyone (and is not
 * already in the agent's home base) and accelerates towards it.
 * If no "uncarried" food can be found, the agent tries to find
 * food carried by a member of the opposing team to accelerate
 * towards.  If no food is visible the agent sets his acceleration
 * to a random vector to search for food.  When it gets close
 * enough to pick up a piece of food, it picks it up and carries
 * it towards its home base corner for 20 time cycles, and drops
 * it again.  The gatherer must then rest for 10 time cycles
 * before picking up another piece of food.  During these 10
 * time cycles, the "exhausted" agent does not accelerate.
 *
 * @author Erik Rasmussen
 */
public class Gatherer extends CarrierAgent
{
	/**
	 * Whether or not the agent is on the team with the home base in the
	 * bottom right-hand corner of the world
	 */
	protected boolean bottomRightTeam;
	/**
	 * The amount of time the gatherer has been carrying a piece of food
	 */
	protected int timeCarried;
	/**
	 * The the amount of time needed to rest
	 */
	protected int timeToRest;
	/**
	 * The vector pointing in the direction of the agent's home base
	 */
	protected Vector towardsHome;

	/**
	 * Constructs a new Gatherer with the given parameters
	 *
	 * @param pickupDistance  The distance from an object the agent must be to
	 *                        pick up the object
	 * @param bottomRightTeam Whether or not this agent is on the team moving
	 *                        food to the bottom right corner of the world
	 * @param size            The agent's radius
	 * @param sight           The distance the agent can see
	 * @param maxSpeed        The maximum speed the agent can travel
	 * @param maxAcceleration The maximum acceleration for this agent
	 */
	public Gatherer(final int pickupDistance, final boolean bottomRightTeam, final int size, final int sight, final double maxSpeed, final double maxAcceleration)
	{
		super(pickupDistance, size, sight, maxSpeed, maxAcceleration);
		this.bottomRightTeam = bottomRightTeam;
		if (bottomRightTeam)
		{
			this.color = Color.cyan;
			towardsHome = new Vector(1, 1);
			towardsHome = towardsHome.setLength(maxAcceleration);
		}
		else
		{
			this.color = Color.pink;
			towardsHome = new Vector(-1, -1);
			towardsHome = towardsHome.setLength(maxAcceleration);
		}
	}

	/**
	 * Observes the world, and follows the Gatherer Algorithm.
	 */
	public void observeWorld()
	{
		timeToRest--;
		if (hasItems())
		{
			timeCarried--;
			setAcceleration(towardsHome);
			if (timeCarried < 0)
				dropFood();
		}
		else if (timeToRest <= 0)
		{
			Food food = chooseFood(false);
			// if no uncarried food found, search for carried food
			if (food == null)
				food = chooseFood(true);
			if (food == null)
				setAcceleration(Vector.getRandom(maxAcceleration));
			else if (world.getVectorToObject(this, food).getLength() <= getPickUpDistance() &&
			         food.getCarriedBy() == null)
			{
				pickUp(food);
				setAcceleration(towardsHome);
			}
			else
				setAcceleration(world.getVectorToObject(this, food));
		}
		else
			setAcceleration(new Vector(0, 0));
	}

	/**
	 * Selects closest visible food not already in home base
	 *
	 * @param carried Whether or not the food we are looking for is being
	 *                carried
	 * @return The closest visible food not already in home base
	 */
	protected Food chooseFood(final boolean carried)
	{
		final List seenObjects = world.getSeenObjects(this);
		final List foods = new ArrayList();
		for (int i = 0; i < seenObjects.size(); i++)
		{
			final WorldObject object = (WorldObject) seenObjects.get(i);
			if (object instanceof Food)
			{
				final Food food = (Food) object;
				if ((carried && food.getCarriedBy() != null && ((Gatherer) food.getCarriedBy()).isBottomRightTeam() != bottomRightTeam) ||
				    (!carried && food.getCarriedBy() == null && !((GatherersWorld) world).isInHome(object, bottomRightTeam)))
					foods.add(object);
			}
		}
		if (!foods.isEmpty())
		{
			// choose closest piece of food
			double minDistance = Double.MAX_VALUE;
			Food closestFood = null;
			for (int i = 0; i < foods.size(); i++)
			{
				final Food food = (Food) foods.get(i);
				final double distance = world.getVectorToObject(this, food).getLength();
				if (distance < minDistance)
				{
					minDistance = distance;
					closestFood = food;
				}
			}
			return closestFood;
		}
		else
			return null;
	}

	/**
	 * Returns the nearest visible gatherer
	 *
	 * @param opponent If true will return nearest visible opponent, otherwise nearest teammate
	 * @return The nearest visible gatherer
	 */
	protected Gatherer getNearestGatherer(final boolean opponent)
	{
		final List seenObjects = world.getSeenObjects(this);
		final List gatherers = new ArrayList();
		for (int i = 0; i < seenObjects.size(); i++)
		{
			final WorldObject object = (WorldObject) seenObjects.get(i);
			if (object instanceof Gatherer &&
			    ((opponent && ((Gatherer) object).bottomRightTeam != bottomRightTeam) ||
			     (!opponent && ((Gatherer) object).bottomRightTeam == bottomRightTeam)))
				gatherers.add(object);
		}
		if (!gatherers.isEmpty())
		{
			// choose nearest opponent
			double minDistance = Double.MAX_VALUE;
			Gatherer nearestGatherer = null;
			for (int i = 0; i < gatherers.size(); i++)
			{
				final Gatherer gatherer = (Gatherer) gatherers.get(i);
				final double distance = world.getVectorToObject(this, gatherer).getLength();
				if (distance < minDistance)
				{
					minDistance = distance;
					nearestGatherer = gatherer;
				}
			}
			return nearestGatherer;
		}
		else
			return null;
	}

	/**
	 * Returns whether or not the agent is on the team with the home base in
	 * the bottom right-hand corner of the world
	 *
	 * @return Whether or not the agent is on the team with the home base in the
	 *         bottom right-hand corner of the world
	 */
	public boolean isBottomRightTeam()
	{
		return bottomRightTeam;
	}

	/**
	 * Attempts to drop any carried food
	 */
	protected void dropFood()
	{
		if (hasItems())
		{
			try
			{
				drop(getInventory()[0]);
				timeToRest = 10;
			}
			catch (CollisionException e)
			{
			}
		}
	}

	/**
	 * Picks up an object
	 *
	 * @param item The object to pick up
	 */
	protected void pickUp(final CarriableObject item)
	{
		super.pickUp(item);
		timeCarried = 20;
	}
}