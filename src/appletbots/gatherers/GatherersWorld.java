/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserved
 */
package appletbots.gatherers;

import appletbots.World;
import appletbots.WorldObject;
import appletbots.geometry.Point;

import java.awt.*;
import java.util.Iterator;

/**
 * This class represents a world for Gatherers to collect food.  It consists
 * of two "home bases" in opposite corners.
 *
 * @author Erik Rasmussen
 */
public class GatherersWorld extends World
{
	/**
	 * The size of the home base zone
	 */
	private int zoneSize;

	/**
	 * Constructs a new gatherers world with the given dimensions
	 *
	 * @param width    The width of the world in pixels
	 * @param height   The height of the world in pixels
	 * @param zoneSize The size of the home base zones measured by the number of
	 *                 pixels from world corners where the zone boundary hits
	 *                 the edge of the world
	 */
	public GatherersWorld(final int width, final int height, final int zoneSize)
	{
		super(width, height);
		this.zoneSize = zoneSize;
	}

	/**
	 * Paints the objects in the world
	 *
	 * @param g The graphics object on which to paint the objects
	 */
	protected void paintObjects(final Graphics g)
	{
		g.setColor(Color.green);
		g.drawLine(0, zoneSize, zoneSize, 0);
		g.drawLine(getWorldWidth() - zoneSize, getWorldHeight(), getWorldWidth(), getWorldHeight() - zoneSize);
		super.paintObjects(g);
	}

	/**
	 * Returns whether or not the given object is in the home base zone of
	 * team specified by the bottomRightTeam boolean parameter
	 *
	 * @param object          The object to check
	 * @param bottomRightTeam Checks the home base zone in the bottom right if
	 *                        true, and the zone in the top left if false
	 * @return Whether or not the given object is in the home base zone of
	 *         team specified by the bottomRightTeam boolean parameter
	 */
	public boolean isInHome(final WorldObject object, final boolean bottomRightTeam)
	{
		final Point location = getData(object).getLocation();
		if (bottomRightTeam)
		{
			return (getWorldWidth() - location.x) + (getWorldHeight() - location.y) < zoneSize;
		}
		else
		{
			return location.x + location.y < zoneSize;
		}
	}

	/**
	 * Called by the WorldThread.  Checks if one team is has won by getting all
	 * the food into its home base zone, and pauses the world if a win has
	 * occurred.  (Also causes World.incrementTime().)
	 */
	public void incrementTime()
	{
		super.incrementTime();
		boolean blueWins = true;
		boolean redWins = true;
		for (Iterator iterator = objectsTable.keySet().iterator(); iterator.hasNext();)
		{
			final WorldObject object = (WorldObject) iterator.next();
			if (object instanceof Food)
			{
				if (isInHome(object, true))
					redWins = false;
				else if (isInHome(object, false))
					blueWins = false;
				else
				{
					blueWins = false;
					redWins = false;
				}
			}
		}
		if (blueWins || redWins)
			stopThread();
	}
}