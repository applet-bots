/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserved
 */
package appletbots.gatherers;

import appletbots.CarriableObject;

import java.awt.*;

/**
 * The class represents a piece of food in the appletbots world
 *
 * @author Erik Rasmussen
 */
public class Food extends CarriableObject
{
	/**
	 * Constructs a food with the given size (radius), maxSpeed, mass, and
	 * color.
	 *
	 * @param size     The radius of the food
	 * @param maxSpeed The maximum speed of the food
	 * @param mass     The mass of the food
	 * @param color    The color of the food
	 */
	public Food(final int size, final double maxSpeed, final double mass, final Color color)
	{
		super(mass, maxSpeed, size, color);
	}
}