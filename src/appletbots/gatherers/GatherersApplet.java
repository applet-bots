/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserved
 */
package appletbots.gatherers;

import appletbots.World;
import appletbots.Applet;
import appletbots.geometry.Point;
import appletbots.geometry.Vector;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Hashtable;

/**
 * An applet to run a Gatherer simulation
 *
 * @author Erik Rasmussen
 */
public class GatherersApplet extends Applet
{
	/**
	 * The maximum number of red agents allowed
	 */
	public static final int MAX_RED_NUM_AGENTS = 50;
	/**
	 * The minimum number of red agents allowed
	 */
	public static final int MIN_RED_NUM_AGENTS = 1;
	/**
	 * The number of red agents
	 */
	protected int numRedAgents = 5;
	/**
	 * The "Number of Red Agents" slider
	 */
	protected JSlider numRedAgentsSlider;
	/**
	 * The "Number of Red Agents" label
	 */
	protected JLabel numRedAgentsLabel;
	/**
	 * The maximum number of blue agents allowed
	 */
	public static final int MAX_BLUE_NUM_AGENTS = 50;
	/**
	 * The minimum number of blue agents allowed
	 */
	public static final int MIN_BLUE_NUM_AGENTS = 1;
	/**
	 * The number of blue agents
	 */
	protected int numBlueAgents = 5;
	/**
	 * The "Number of Blue Agents" slider
	 */
	protected JSlider numBlueAgentsSlider;
	/**
	 * The "Number of Blue Agents" label
	 */
	protected JLabel numBlueAgentsLabel;
	/**
	 * The maximum agent sight value allowed
	 */
	public static final int MAX_AGENT_SIGHT = 100;
	/**
	 * The minimum agent sight value allowed
	 */
	public static final int MIN_AGENT_SIGHT = 5;
	/**
	 * The agent sight value
	 */
	protected int agentSight = 60;
	/**
	 * The "Agent Sight" slider
	 */
	protected JSlider agentSightSlider;
	/**
	 * The "Agent Sight" label
	 */
	protected JLabel agentSightLabel;
	/**
	 * The maximum number of food allowed
	 */
	public static final int MAX_NUM_FOOD = 25;
	/**
	 * The minimum number of food allowed
	 */
	public static final int MIN_NUM_FOOD = 1;
	/**
	 * The number of food
	 */
	protected int numFood = 10;
	/**
	 * The "Number of Food" slider
	 */
	protected JSlider numFoodSlider;
	/**
	 * The "Number of Food" label
	 */
	protected JLabel numFoodLabel;
	/**
	 * The "Stationary Food" checkbox
	 */
	protected JCheckBox stationaryFoodCheckBox;
	/**
	 * Whether or not the food is stationary
	 */
	protected boolean stationaryFood = true;

	/**
	 * Initializes the world with the appropriate amount of food and
	 * Gatherers with the appropriate "agent sight" settings
	 *
	 * @return A world with Gatherers
	 */
	protected World initializeWorld()
	{
		final GatherersWorld world = new GatherersWorld(300, 300, 100);
		try
		{
			for (int i = 0; i < numFood; i++)
				world.addObject(new Food(2, (stationaryFood ? 0 : 1), 10, Color.red), new Point(world.getWorldWidth() / 2 - (numFood * 5 / 2) + i * 5, world.getWorldHeight() / 2), new Vector(0, 0));
			for (int i = 0; i < numBlueAgents; i++)
			{
				world.addObject(new Gatherer(5, true, 5, agentSight, 5, 3));
			}
			for (int i = 0; i < numRedAgents; i++)
			{
				world.addObject(new Gatherer(5, false, 5, agentSight, 5, 3));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return world;
	}

	/**
	 * Returns a settings panel with "Number of Agents", "Number of Food",
	 * and "Agent Sight" sliders
	 *
	 * @return A settings panel with "Number of Agents", "Number of Food",
	 *         and "Agent Sight" sliders
	 */
	protected JPanel getSettingsPanel()
	{
		final JPanel settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));

		numRedAgentsLabel = new JLabel("Number of Red Agents: " + numRedAgents);
		numRedAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numRedAgentsLabel);

		final Hashtable numRedAgentsLabels = new Hashtable();
		numRedAgentsLabels.put(new Integer(MIN_RED_NUM_AGENTS), new JLabel(Integer.toString(MIN_RED_NUM_AGENTS)));
		numRedAgentsLabels.put(new Integer(MAX_RED_NUM_AGENTS), new JLabel(Integer.toString(MAX_RED_NUM_AGENTS)));

		numRedAgentsSlider = new JSlider(MIN_RED_NUM_AGENTS, MAX_RED_NUM_AGENTS, numRedAgents);
		numRedAgentsSlider.setLabelTable(numRedAgentsLabels);
		numRedAgentsSlider.setPaintLabels(true);
		numRedAgentsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numRedAgentsChanged(event);
			}
		});
		settingsPanel.add(numRedAgentsSlider);

		numBlueAgentsLabel = new JLabel("Number of Blue Agents: " + numBlueAgents);
		numBlueAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numBlueAgentsLabel);

		final Hashtable numBlueAgentsLabels = new Hashtable();
		numBlueAgentsLabels.put(new Integer(MIN_BLUE_NUM_AGENTS), new JLabel(Integer.toString(MIN_BLUE_NUM_AGENTS)));
		numBlueAgentsLabels.put(new Integer(MAX_BLUE_NUM_AGENTS), new JLabel(Integer.toString(MAX_BLUE_NUM_AGENTS)));

		numBlueAgentsSlider = new JSlider(MIN_BLUE_NUM_AGENTS, MAX_BLUE_NUM_AGENTS, numBlueAgents);
		numBlueAgentsSlider.setLabelTable(numBlueAgentsLabels);
		numBlueAgentsSlider.setPaintLabels(true);
		numBlueAgentsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numBlueAgentsChanged(event);
			}
		});
		settingsPanel.add(numBlueAgentsSlider);

		agentSightLabel = new JLabel("Agent Sight: " + agentSight);
		agentSightLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(agentSightLabel);

		final Hashtable agentSightLabels = new Hashtable();
		agentSightLabels.put(new Integer(MIN_AGENT_SIGHT), new JLabel(Integer.toString(MIN_AGENT_SIGHT)));
		agentSightLabels.put(new Integer(MAX_AGENT_SIGHT), new JLabel(Integer.toString(MAX_AGENT_SIGHT)));

		agentSightSlider = new JSlider(MIN_AGENT_SIGHT, MAX_AGENT_SIGHT, agentSight);
		agentSightSlider.setLabelTable(agentSightLabels);
		agentSightSlider.setPaintLabels(true);
		agentSightSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				agentSightChanged(event);
			}
		});
		settingsPanel.add(agentSightSlider);

		numFoodLabel = new JLabel("Number of Food: " + numFood);
		numFoodLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numFoodLabel);

		final Hashtable numFoodLabels = new Hashtable();
		numFoodLabels.put(new Integer(MIN_NUM_FOOD), new JLabel(Integer.toString(MIN_NUM_FOOD)));
		numFoodLabels.put(new Integer(MAX_NUM_FOOD), new JLabel(Integer.toString(MAX_NUM_FOOD)));

		numFoodSlider = new JSlider(MIN_NUM_FOOD, MAX_NUM_FOOD, numFood);
		numFoodSlider.setLabelTable(numFoodLabels);
		numFoodSlider.setPaintLabels(true);
		numFoodSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numFoodChanged(event);
			}
		});
		settingsPanel.add(numFoodSlider);

		stationaryFoodCheckBox = new JCheckBox("Stationary Food");
		stationaryFoodCheckBox.setSelected(stationaryFood);
		stationaryFoodCheckBox.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		stationaryFoodCheckBox.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				GatherersApplet.this.stationaryFoodClicked(event);
			}
		});
		settingsPanel.add(stationaryFoodCheckBox);

		final JLabel changes = new JLabel("Changes will take");
		changes.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		final JLabel takeEffect = new JLabel("effect at next reset");
		takeEffect.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(changes);
		settingsPanel.add(takeEffect);
		return settingsPanel;
	}

	/**
	 * The method invoked when the "Number of Red Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numRedAgentsChanged(final ChangeEvent event)
	{
		numRedAgents = numRedAgentsSlider.getValue();
		numRedAgentsLabel.setText("Number of Red Agents: " + numRedAgents);
	}

	/**
	 * The method invoked when the "Number of Blue Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numBlueAgentsChanged(final ChangeEvent event)
	{
		numBlueAgents = numBlueAgentsSlider.getValue();
		numBlueAgentsLabel.setText("Number of Blue Agents: " + numBlueAgents);
	}

	/**
	 * The method invoked when the "Agent Sight" slider changes
	 *
	 * @param event The change event
	 */
	public void agentSightChanged(final ChangeEvent event)
	{
		agentSight = agentSightSlider.getValue();
		agentSightLabel.setText("Agent Sight: " + agentSight);
	}

	/**
	 * The method invoked when the "Number of Food" slider changes
	 *
	 * @param event The change event
	 */
	public void numFoodChanged(final ChangeEvent event)
	{
		numFood = numFoodSlider.getValue();
		numFoodLabel.setText("Number of Food: " + numFood);
	}

	/**
	 * The method invoked when the user selects or deselects the "Stationary
	 * Food" check box
	 *
	 * @param event The action event
	 */
	public void stationaryFoodClicked(final ActionEvent event)
	{
		stationaryFood = stationaryFoodCheckBox.isSelected();
	}
}