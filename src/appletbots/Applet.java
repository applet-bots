/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserved
 */
package appletbots;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Hashtable;

/**
 * This is an abstract applet class to run appletbots worlds.  Subclasses need
 * only to implement the initializeWorld() method.  Optionally, they can
 * override the getSettingsPanel() method to all define a settings panel.
 *
 * @author Erik Rasmussen
 */
public abstract class Applet extends JApplet
{
	/**
	 * The number of milliseconds to wait between time field updates.  If the
	 * time cycles are flying by too quickly, the applet cannot paint the time
	 * field quickly enough, so it waits until a minimum number of milliseconds
	 * have passed since the last update before updating the time field.
	 */
	protected static final long MILLISECONDS_BETWEEN_TIME_FIELD_UPDATES = 100;
	/**
	 * The world
	 */
	private World world;
	/**
	 * The speed slider
	 */
	private JSlider speedSlider;
	/**
	 * The start button
	 */
	private JButton startButton;
	/**
	 * The stop button
	 */
	private JButton stopButton;
	/**
	 * The reset button
	 */
	private JButton resetButton;
	/**
	 * The time field
	 */
	private JTextField timeField;
	/**
	 * The settings panel
	 */
	protected JPanel settingsPanel;
	/**
	 * The object viewer panel
	 */
	protected ObjectViewer objectViewer;
	/**
	 * The long integer representation of the last time the time field was
	 * updated
	 */
	private long lastTimeUpdatedTimeField = 0;

	/**
	 * Constructs the applet
	 */
	public Applet()
	{
	}

	/**
	 * Initializes the applet
	 */
	public void init()
	{
		getContentPane().setLayout(new BorderLayout(10, 10));

		getContentPane().add(Box.createRigidArea(new Dimension(5, 5)), BorderLayout.NORTH);
		getContentPane().add(Box.createRigidArea(new Dimension(5, 5)), BorderLayout.SOUTH);
		getContentPane().add(Box.createRigidArea(new Dimension(5, 5)), BorderLayout.EAST);
		getContentPane().add(Box.createRigidArea(new Dimension(5, 5)), BorderLayout.WEST);

		final Box mainBox = new Box(BoxLayout.X_AXIS);

		final Box worldAndControls = new Box(BoxLayout.Y_AXIS);
		world = initializeWorld();
		world.addMouseListener(new java.awt.event.MouseAdapter()
		{
			public void mouseClicked(final MouseEvent event)
			{
				worldClicked(event);
			}
		});

		worldAndControls.add(world);
		worldAndControls.add(Box.createRigidArea(new Dimension(5, 5)));

		startButton = new JButton("Start");
		startButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				Applet.this.startClicked(event);
			}
		});

		stopButton = new JButton("Stop");
		stopButton.setEnabled(false);
		stopButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				Applet.this.stopClicked(event);
			}
		});

		resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				Applet.this.resetClicked(event);
			}
		});

		final Box controlsBox = new Box(BoxLayout.X_AXIS);
		controlsBox.add(startButton);
		controlsBox.add(stopButton);
		controlsBox.add(resetButton);
		worldAndControls.add(controlsBox);

		speedSlider = new JSlider(0, 1000, 500);
		speedSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				speedChanged(event);
			}
		});
		final Hashtable sliderLabels = new Hashtable();
		sliderLabels.put(new Integer(950), new JLabel("Slower"));
		sliderLabels.put(new Integer(50), new JLabel("Faster"));
		speedSlider.setLabelTable(sliderLabels);
		speedSlider.setPaintLabels(true);
		speedSlider.setInverted(true);
		worldAndControls.add(speedSlider);

		timeField = new JTextField(10);
		timeField.setMaximumSize(new java.awt.Dimension(60, timeField.getMinimumSize().height));
		timeField.setEditable(false);

		final Box timeFieldBox = new Box(BoxLayout.X_AXIS);
		timeFieldBox.add(new JLabel("Time"));
		timeFieldBox.add(timeField);
		worldAndControls.add(timeFieldBox);

		worldAndControls.add(Box.createRigidArea(new java.awt.Dimension(world.getWorldWidth(), 1)));
		mainBox.add(worldAndControls);
		mainBox.add(Box.createRigidArea(new java.awt.Dimension(1, 500)));

		settingsPanel = getSettingsPanel();
		settingsPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		final JPanel settingsInnerBox = new JPanel();
		settingsInnerBox.setBorder(BorderFactory.createEtchedBorder());
		settingsInnerBox.add(settingsPanel);

		final JLabel settingsLabel = new JLabel("Settings");
		settingsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);

		final Box settingsOuterBox = new Box(BoxLayout.Y_AXIS);
		settingsOuterBox.add(settingsLabel);
		settingsOuterBox.add(settingsInnerBox);

		objectViewer = new ObjectViewer();
		objectViewer.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		final JPanel viewerInnerBox = new JPanel();
		viewerInnerBox.setBorder(BorderFactory.createEtchedBorder());
		viewerInnerBox.add(objectViewer);

		final JLabel viewerLabel = new JLabel("Object Viewer");
		viewerLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);

		settingsOuterBox.add(viewerLabel);
		settingsOuterBox.add(viewerInnerBox);

		mainBox.add(settingsOuterBox);

		getContentPane().add(mainBox, BorderLayout.CENTER);

		new TimeUpdaterThread(this).start();
	}

	/**
	 * Stops any threads still running
	 */
	public void destroy()
	{
		world.stopThread();
	}

	/**
	 * The method invoked when the start button is clicked.  Starts the world
	 * thread.
	 *
	 * @param event The action event
	 */
	public void startClicked(final ActionEvent event)
	{
		world.startThread();
		startButton.setEnabled(false);
		stopButton.setEnabled(true);
	}

	/**
	 * The method invoked when the stop button is clicked.  Stops the world
	 * thread.
	 *
	 * @param event The action event
	 */
	public void stopClicked(final ActionEvent event)
	{
		world.stopThread();
		startButton.setEnabled(true);
		stopButton.setEnabled(false);
	}

	/**
	 * The method invoked when the reset button is clicked.  Resets the world
	 * thread.
	 *
	 * @param event The action event
	 */
	public void resetClicked(final ActionEvent event)
	{
		final boolean wasRunning = world.isRunning();
		if (wasRunning)
			world.stopThread();
		world.importObjects(initializeWorld());
		world.setDelay(speedSlider.getValue());
		if (wasRunning)
			world.startThread();
		world.repaint();
		objectViewer.setData(null);
	}

	/**
	 * The method invoked when the speed slider is clicked.  Calls
	 * world.setDelay(long).
	 *
	 * @param event The change event
	 */
	public void speedChanged(final ChangeEvent event)
	{
		world.setDelay(speedSlider.getValue());
	}

	/**
	 * Returns the number of milliseconds between time cycles
	 *
	 * @return The number of milliseconds between time cycles
	 */
	public int getDelay()
	{
		return world.getDelay();
	}

	/**
	 * Updates the time field if the time field has not been updated in at
	 * least MILLISECONDS_BETWEEN_TIME_FIELD_UPDATES milliseconds.
	 */
	public void updateTimeField()
	{
		// only update every half second
		final long currentTime = System.currentTimeMillis();
		if (currentTime - lastTimeUpdatedTimeField > MILLISECONDS_BETWEEN_TIME_FIELD_UPDATES)
		{
			timeField.setText(new Long(world.getTime()).toString());
			objectViewer.update();
			world.setSelectedSight(objectViewer.getShowSight() ? Color.green : null);
			lastTimeUpdatedTimeField = currentTime;
		}
	}

	/**
	 * The method invoked when the user clicks on the world.  It calls
	 * world.selectObjectAt(x,y).
	 *
	 * @param event The mouse event
	 */
	public void worldClicked(final MouseEvent event)
	{
		final WorldObject object = world.getObjectAt(event.getX(), event.getY());
		if (object != null)
			objectViewer.setData(world.getData(object));
		else
			objectViewer.setData(null);
		world.selectObject(object);
	}

	/**
	 * Returns a blank settings panel.  This method may be overridden by
	 * subclasses to provide a user interface for changing world initialization
	 * settings.
	 *
	 * @return A blank settings panel
	 */
	protected JPanel getSettingsPanel()
	{
		return new JPanel();
	}

	/**
	 * Returns an initialized world with the desired agents and object added.
	 *
	 * @return An initialized world with the desired agents and object added.
	 */
	protected abstract World initializeWorld();
}