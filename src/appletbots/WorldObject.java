/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

import java.awt.*;

/**
 * This is an abstract class representing an object in the world
 *
 * @author Erik Rasmussen
 */
public abstract class WorldObject
{
	/**
	 * The object's mass
	 */
	protected double mass;
	/**
	 * The object's maximum speed
	 */
	protected double maxSpeed;
	/**
	 * The object's radius
	 */
	protected int size;
	/**
	 * The object's color
	 */
	protected Color color;

	/**
	 * Creates a new WorldObject with the given parameters
	 *
	 * @param mass     The object's mass
	 * @param maxSpeed The object's maximum speed
	 * @param size     The object's radius
	 * @param color    The object's color
	 */
	public WorldObject(final double mass, final double maxSpeed, final int size, final Color color)
	{
		this.mass = mass;
		this.maxSpeed = maxSpeed;
		this.size = size;
		this.color = color;
	}

	/**
	 * Returns the object's mass
	 *
	 * @return The object's mass
	 */
	public final double getMass()
	{
		return mass;
	}

	/**
	 * Returns the object's maximum speed
	 *
	 * @return The object's maximum speed
	 */
	public final double getMaxSpeed()
	{
		return maxSpeed;
	}

	/**
	 * Returns the object's radius
	 *
	 * @return The object's radius
	 */
	public final int getSize()
	{
		return size;
	}

	/**
	 * Returns the object's color
	 *
	 * @return The object's color
	 */
	public final Color getColor()
	{
		return color;
	}

	/**
	 * Informs the object that it has been in a collision
	 *
	 * @param object The object collided with
	 */
	public void collidedWith(final WorldObject object)
	{
	}
}