/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.geometry;

/**
 * This class represents a point in two-dimensional space
 *
 * @author Erik Rasmussen
 */
public class Point
{
	/**
	 * The x-coordinate
	 */
	public double x;
	/**
	 * The y-coordinate
	 */
	public double y;

	/**
	 * Creates a new point with the given coordinates
	 *
	 * @param x The x-coordinate
	 * @param y The y-coordinate
	 */
	public Point(final double x, final double y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Returns the distance between this point and another point
	 *
	 * @param point Another point
	 * @return The distance between this point and another point
	 */
	public double distance(final Point point)
	{
		final double px = point.x - x;
		final double py = point.y - y;
		return Math.sqrt(px * px + py * py);
	}

	/**
	 * Returns the result of adding a vector to this point
	 *
	 * @param vector The vector to add
	 * @return The result of adding a vector to this point
	 */
	public Point add(final Vector vector)
	{
		return new Point(x + vector.x, y + vector.y);
	}

	/**
	 * Returns whether or not this point equals another point
	 *
	 * @param obj Another point
	 * @return Whether or not this point equals another point
	 */
	public boolean equals(final Object obj)
	{
		if (obj instanceof Point)
		{
			final Point that = (Point) obj;
			return this.x == that.x && this.y == that.y;
		}
		return false;
	}

	/**
	 * Returns a copy of this point
	 *
	 * @return A copy of this point
	 */
	public Object clone()
	{
		return new Point(x, y);
	}

	/**
	 * Returns a string representation of this point
	 *
	 * @return A string representation of this point
	 */
	public String toString()
	{
		return "(" + x + "," + y + ")";
	}
}