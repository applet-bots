/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.geometry;

import java.text.NumberFormat;
import java.util.Collection;
import java.util.Iterator;

/**
 * This class represents an immutable two-dimensional vector
 *
 * @author Erik Rasmussen
 */
public class Vector implements Comparable
{
	/**
	 * The x-component of the vector
	 */
	public double x;
	/**
	 * The y-component of the vector
	 */
	public double y;

	/**
	 * A zero vector
	 */
	public static Vector ZERO = new Vector(0, 0);
	/**
	 * A normalized vector in the direction of the x-axis
	 */
	public static Vector X_AXIS = new Vector(1, 0);
	/**
	 * A normalized vector in the direction of the y-axis
	 */
	public static Vector Y_AXIS = new Vector(0, 1);

	/**
	 * A small value that doubles, such that doubles that are this close are deemed equal.
	 */
	private static final double EPSILON = 0.0000000001; // 1 * 10^-10

	private static NumberFormat FORMATTER = NumberFormat.getInstance();
	static
	{
		FORMATTER.setMaximumFractionDigits(8);
	}

	/**
	 * Creates a new vector with the given x and y components
	 *
	 * @param x The x-component of the vector
	 * @param y The y-component of the vector
	 */
	public Vector(final double x, final double y)
	{
		if (!isValidDouble(x))
			throw new IllegalArgumentException("Invalid value (" + x + ") for x component");
		if (!isValidDouble(y))
			throw new IllegalArgumentException("Invalid value (" + y + ") for y component");
		this.x = x;
		this.y = y;
	}

	/**
	 * Constructs a new vector from one point to another
	 *
	 * @param start The start point
	 * @param end   The end point
	 */
	public Vector(final Point start, final Point end)
	{
		this(end.x - start.x, end.y - start.y);
	}

	/**
	 * Returns a random vector of the given length
	 *
	 * @param length The length of the vector
	 * @return A random vector of the given length
	 */
	public static Vector getRandom(final double length)
	{
		if (!isValidDouble(length))
			throw new IllegalArgumentException("length (" + length + ") is an invalid double value");
		final Vector v = new Vector(Math.random() - 0.5, Math.random() - 0.5);
		return v.setLength(length);
	}

	/**
	 * Returns the average of a collection of vectors
	 *
	 * @param vectors A collection of vectors
	 * @return The average of the collection of vectors
	 */
	public static Vector average(final Collection vectors)
	{
		if (vectors.isEmpty())
			return ZERO;
		else
		{
			Vector sum = ZERO;
			for (Iterator iterator = vectors.iterator(); iterator.hasNext();)
			{
				final Vector v = (Vector) iterator.next();
				sum = sum.add(v);
			}
			return sum.divide(vectors.size());
		}
	}

	/**
	 * Returns the length of the vector
	 *
	 * @return The length of the vector
	 */
	public double getLength()
	{
		return Math.sqrt(x * x + y * y);
	}

	/**
	 * Creates a new vector aligned with this one with the given length
	 *
	 * @param length The length of the new vector
	 * @return A new vector aligned with this one with the given length
	 */
	public Vector setLength(final double length)
	{
		if (!isValidDouble(length))
			throw new IllegalArgumentException("length (" + length + ") is an invalid double value");
		final double currentLength = getLength();
		if (currentLength != 0)
		{
			return new Vector(x * length / currentLength, y * length / currentLength);
		}
		else
		{
			// you can't set the length of a zero vector
			// because you don't know what direction
			return (Vector) clone();
		}
	}

	/**
	 * Creates a new vector aligned with this one with a length of 1
	 *
	 * @return A new vector aligned with this one with a length of 1
	 */
	public Vector normalize()
	{
		return setLength(1);
	}

	/**
	 * Returns the result of multiplying this vector times a scalar number
	 *
	 * @param multiplicand The scalar number to multiply the vector by
	 * @return The result of multiplying this vector times a scalar number
	 */
	public Vector multiply(final double multiplicand)
	{
		if (!isValidDouble(multiplicand))
			throw new IllegalArgumentException("multiplicand (" + multiplicand + ") is an invalid double value");
		return setLength(getLength() * multiplicand);
	}

	/**
	 * Returns the result of dividinig this vector by a scalar number
	 *
	 * @param divisor The scalar number to divide the vector by
	 * @return The result of dividing this vector by a scalar number
	 */
	public Vector divide(final double divisor)
	{
		if (equals(divisor, 0))
			throw new IllegalArgumentException("divide by zero!");
		return setLength(getLength() / divisor);
	}

	/**
	 * Returns the result of adding this vector to another vector
	 *
	 * @param v Another vector
	 * @return The result of adding this vector to another vector
	 */
	public Vector add(final Vector v)
	{
		return new Vector(x + v.x, y + v.y);
	}

	/**
	 * Returns the result of subtracting another vector from this vector
	 *
	 * @param v Another vector
	 * @return The result of subtracting another vector from this vector
	 */
	public Vector subtract(final Vector v)
	{
		return new Vector(x - v.x, y - v.y);
	}

	/**
	 * Returns the dot product of this vector and another vector
	 *
	 * @param v Another vector
	 * @return The dot product of this vector and another vector
	 */
	public double dotProduct(final Vector v)
	{
		return x * v.x + y * v.y;
	}

	/**
	 * Returns the result of rotating this vector by theta radians
	 *
	 * @param theta The number of radians to rotate the vector
	 * @return The result of rotating this vector by theta radians
	 */
	public Vector rotate(final double theta)
	{
		if (!isValidDouble(theta))
			throw new IllegalArgumentException("theta (" + theta + ") is an invalid double value");
		return new Vector(x * Math.cos(-theta) - y * Math.sin(-theta),
		                  x * Math.sin(-theta) + y * Math.cos(-theta));
	}

	/**
	 * Returns the angle (in radians) from this vector to the given vector
	 *
	 * @param v Another vector
	 * @return The angle (in radians) from this vector to the given vector
	 */
	public double getAngleToVector(final Vector v)
	{
		if(equals(v))
			return 0;
		final double angle = Math.acos(normalize().dotProduct(v.normalize()));
		// determine sign
		if(rotate(angle).isAligned(v))
			return angle;
		else if(rotate(-angle).isAligned(v))
			return -angle;
		else
			throw new IllegalStateException("Error calculating angle between "+normalize()+" and "+v.normalize()+".  " +
			                                "Tried "+angle+" (giving "+rotate(angle).normalize()+") and "+(-angle)+" " +
			                                "(giving "+rotate(-angle).normalize()+"), but neither worked.");
	}

	/**
	 * Returns whether or not this vector is parellel to the given vector
	 *
	 * @param v Another vector
	 * @return Whether or not this vector is parellel to the given vector
	 */
	public boolean isParallel(final Vector v)
	{
		return equals(Math.abs(dotProduct(v)), v.getLength());
	}

	/**
	 * Returns whether or not this vector is aligned (pointing
	 * in same direction) to the given vector
	 *
	 * @param v Another vector
	 * @return Whether or not this vector is aligned (pointing
	 *         in same direction) to the given vector
	 */
	public boolean isAligned(final Vector v)
	{
		return normalize().equals(v.normalize());
	}

	/**
	 * Returns whether or not this vector equals another vector
	 *
	 * @param obj Another vector
	 * @return Whether or not this vector equals another vector
	 */
	public boolean equals(final Object obj)
	{
		if (obj instanceof Vector)
		{
			final Vector that = (Vector) obj;
			return equals(this.x, that.x) && equals(this.y, that.y);
		}
		return false;
	}

	/**
	 * Returns a copy of this vector
	 *
	 * @return A copy of this vector
	 */
	public Object clone()
	{
		return new Vector(x, y);
	}

	/**
	 * Returns a string representation of this vector
	 *
	 * @return A string representation of this vector
	 */
	public String toString()
	{
		final StringBuffer buffer = new StringBuffer();
		buffer.append("(");
		buffer.append(FORMATTER.format(x));
		buffer.append(",");
		buffer.append(FORMATTER.format(y));
		buffer.append(")");
		return buffer.toString();
	}

	/**
	 * Returns whether or on the given value is a valid double for
	 * use in vector math
	 *
	 * @param value The double value to check
	 * @return true, if the given value is a valid double, false otherwise
	 */
	private static boolean isValidDouble(final double value)
	{
		return !Double.isNaN(value) && !Double.isInfinite(value);
	}

	/**
	 * Compares this vector to another based solely on magnitude.
	 *
	 * @param o the Object to be compared.
	 * @return a negative integer, zero, or a positive integer as this object
	 *         is less than, equal to, or greater than the specified object.
	 * @throws ClassCastException if the specified object's type prevents it
	 *                            from being compared to this Object.
	 */
	public int compareTo(final Object o)
	{
		return Double.compare(getLength(), ((Vector) o).getLength());
	}

	/**
	 * Compares two double values using their string representation.
	 * @param d1
	 * @param d2
	 * @return true, if the double values are close enough to equal, false otherwise
	 */
	private static boolean equals(final double d1, final double d2)
	{
		return Math.abs(d1 - d2) <= EPSILON;
	}
}