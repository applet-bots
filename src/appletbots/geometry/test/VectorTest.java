package appletbots.geometry.test;

import appletbots.geometry.Vector;
import junit.framework.TestCase;

/**
 * A JUnit test for the vector class.
 */
public class VectorTest extends TestCase
{
	private static final double DEGREES_180 = Math.PI;
	private static final double DEGREES_360 = 2 * DEGREES_180;
	private static final double DEGREES_90 = DEGREES_180 / 2;
	private static final double DEGREES_45 = DEGREES_90 / 2;

	private static final double DELTA = Math.pow(1, -10);

	public void testGetLength() throws Exception
	{
		assertEquals(Math.sqrt(2), new Vector(1, 1).getLength(), DELTA);
		assertEquals(1, Vector.X_AXIS.getLength(), DELTA);
		assertEquals(1, Vector.Y_AXIS.getLength(), DELTA);
		assertEquals(5, new Vector(3, 4).getLength(), DELTA);
	}

	public void testRotate() throws Exception
	{
		assertEquals(Vector.X_AXIS.rotate(DEGREES_180), Vector.X_AXIS.multiply(-1));
		assertEquals(Vector.Y_AXIS.rotate(DEGREES_90), Vector.X_AXIS);
		assertEquals(Vector.X_AXIS.rotate(-DEGREES_90), Vector.Y_AXIS);
		assertEquals(Vector.X_AXIS.rotate(DEGREES_90), Vector.Y_AXIS.multiply(-1));
		assertEquals(Vector.X_AXIS.rotate(DEGREES_360), Vector.X_AXIS);
		assertEquals(Vector.Y_AXIS.rotate(-DEGREES_90), Vector.X_AXIS.multiply(-1));
		assertEquals(Vector.Y_AXIS.rotate(DEGREES_180 + DEGREES_90), Vector.X_AXIS.multiply(-1));
		assertTrue(Vector.Y_AXIS.rotate(DEGREES_45)+" not aligned with "+new Vector(1,1), Vector.Y_AXIS.rotate(DEGREES_45).isAligned(new Vector(1, 1)));
		assertTrue(Vector.Y_AXIS.rotate(-DEGREES_90 - DEGREES_45)+" not parallel with "+new Vector(1, 1), Vector.Y_AXIS.rotate(-DEGREES_90 - DEGREES_45).isParallel(new Vector(1, 1)));
	}

	public void testGetAngleToVector() throws Exception
	{
		assertEquals(-DEGREES_90, Vector.X_AXIS.getAngleToVector(Vector.Y_AXIS), DELTA);
		assertEquals(DEGREES_90, Vector.Y_AXIS.getAngleToVector(Vector.X_AXIS), DELTA);
		assertEquals(DEGREES_90, Vector.X_AXIS.getAngleToVector(Vector.Y_AXIS.multiply(-1)), DELTA);
		assertEquals(-DEGREES_90, Vector.X_AXIS.multiply(-1).getAngleToVector(Vector.Y_AXIS.multiply(-1)), DELTA);
		assertEquals(DEGREES_90, Vector.X_AXIS.multiply(-1).getAngleToVector(Vector.Y_AXIS), DELTA);
	}

	public void testRotateToAlign() throws Exception
	{
		for(int i=0;i<100;i++)
		{
			final Vector a = Vector.getRandom(1);
			final Vector b = Vector.getRandom(2);
			final double thetaToAlign = a.getAngleToVector(b);
			final Vector aligned = a.rotate(thetaToAlign);
			assertTrue("Rotated "+a+" by "+thetaToAlign+" radians to align with "+b+", but ended up with "+aligned+", which is not aligned. (i="+i+")", aligned.isAligned(b));
		}
	}
}