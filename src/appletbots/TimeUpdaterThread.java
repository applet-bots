/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

/**
 * This thread is used by the appletbots applet to update its components after
 * each time cycle
 *
 * @author Erik Rasmussen
 */
public class TimeUpdaterThread extends Thread
{
	/**
	 * The applet this thread is running for
	 */
	private Applet applet;

	/**
	 * Constructs a new TimeUpdaterThread for the given applet
	 *
	 * @param applet The applet the thread is to run for
	 */
	public TimeUpdaterThread(final Applet applet)
	{
		this.applet = applet;
	}

	/**
	 * Simply calls applet.updateTimeField() and sleeps for applet.getDelay()
	 * milliseconds
	 */
	public void run()
	{
		while (true)
		{
			applet.updateTimeField();
			try
			{
				Thread.sleep(applet.getDelay());
			}
			catch (InterruptedException e)
			{
				break;
			}
		}
	}
}