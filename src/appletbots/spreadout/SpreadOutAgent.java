/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.spreadout;

import appletbots.Agent;
import appletbots.geometry.Vector;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


/**
 * A spread-out agent looks at its neighbors, and accelerates to a position
 * to maximize the distance to his neighbors.
 *
 * @author Erik Rasmussen
 */
public class SpreadOutAgent extends Agent
{
	/**
	 * Percentage of desired location to accelerate
	 */
	private double accelerationFactor;

	/**
	 * Whether or not to stop when there are no visible neighbors
	 */
	private boolean stopWhenNoNeighbors;

	/**
	 * Constructs a spread-out agent with the given parameters
	 *
	 * @param size                       The agent's radius
	 * @param sight                      The distance the agent can see
	 * @param maxSpeed                   The maximum speed the agent can travel
	 * @param maxAcceleration            The maximum acceleration for this agent
	 * @param accelerationFactor         Percentage of desired location to accelerate
	 * @param stopWhenNoNeighbors        Whether or not to stop when there are no visible neighbors
	 */
	public SpreadOutAgent(final int size, final int sight, final double maxSpeed, final double maxAcceleration, final double accelerationFactor, final boolean stopWhenNoNeighbors)
	{
		super(size, sight, maxSpeed, maxAcceleration);
		this.accelerationFactor = accelerationFactor;
		this.stopWhenNoNeighbors = stopWhenNoNeighbors;
		this.color = Color.cyan;
	}

	/**
	 * Observes the world, and follows the Spread-Out Agent Algorithm.
	 */
	public void observeWorld()
	{
		final List neighbors = world.getNeighbors(this);
		if(neighbors.isEmpty() && stopWhenNoNeighbors)
			setAcceleration(world.getVelocity(this).multiply(-1));  // stop
		else
		{
			final Collection vectorsToNeighbors = new ArrayList(neighbors.size());
			for (Iterator iterator = neighbors.iterator(); iterator.hasNext();)
			{
				Agent neighbor = (Agent) iterator.next();
				vectorsToNeighbors.add(world.getVectorToObject(this, neighbor));
			}
			Vector averageVectorToNeighbors = Vector.average(vectorsToNeighbors);
			setAcceleration(averageVectorToNeighbors.multiply(-1 * accelerationFactor));
		}
	}
}