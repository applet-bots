/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.spreadout;

import appletbots.World;
import appletbots.Applet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;
import java.util.Hashtable;

/**
 * An applet to run a SpreadOutAgent simulation
 *
 * @author Erik Rasmussen
 */
public class SpreadOutApplet extends Applet
{
	/**
	 * The maximum number of agents allowed
	 */
	public static final int MAX_NUM_AGENTS = 100;
	/**
	 * The minimum number of agents allowed
	 */
	public static final int MIN_NUM_AGENTS = 2;
	/**
	 * The number of agents
	 */
	protected int numAgents = 50;
	/**
	 * The "Number of Agents" slider
	 */
	protected JSlider numAgentsSlider;
	/**
	 * The "Number of Agents" label
	 */
	protected JLabel numAgentsLabel;
	/**
	 * The maximum agent sight value allowed
	 */
	public static final int MAX_AGENT_SIGHT = 100;
	/**
	 * The minimum agent sight value allowed
	 */
	public static final int MIN_AGENT_SIGHT = 5;
	/**
	 * The agent sight value
	 */
	protected int agentSight = 60;
	/**
	 * The "Agent Sight" slider
	 */
	protected JSlider agentSightSlider;
	/**
	 * The "Agent Sight" label
	 */
	protected JLabel agentSightLabel;
	/**
	 * The maximum acceleration factor allowed
	 */
	public static final int MAX_ACCELERATION_FACTOR = 100;
	/**
	 * The minimum acceleration factor allowed
	 */
	public static final int MIN_ACCELERATION_FACTOR = 0;
	/**
	 * The acceleration factor
	 */
	protected int accelerationFactor = 10;
	/**
	 * The "Acceleration Factor" slider
	 */
	protected JSlider accelerationFactorSlider;
	/**
	 * The "Acceleration Factor" label
	 */
	protected JLabel accelerationFactorLabel;
	/**
	 * The "Stop When No Visible Neighbors" checkbox
	 */
	protected JCheckBox stopWhenNoNeighborsCheckBox;
	/**
	 * Whether or not to stop when there are no visible neighbors
	 */
	protected boolean stopWhenNoNeighbors = true;

	private static final NumberFormat PERCENT_FORMAT = NumberFormat.getPercentInstance();

	/**
	 * Initializes the world with the appropriate number of SpreadOutAgents
	 * with the appropriate "agent sight" settings
	 *
	 * @return A world with SpreadOutAgents
	 */
	protected World initializeWorld()
	{
		final World world = new World(300, 300);
		try
		{
			for (int i = 0; i < numAgents; i++)
				world.addObject(new SpreadOutAgent(5, agentSight, 5, 3, accelerationFactor, stopWhenNoNeighbors));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return world;
	}

	/**
	 * Returns a settings panel with "Number of Agents" and "Agent Sight"
	 * sliders
	 *
	 * @return A settings panel with "Number of Agents" and "Agent Sight"
	 *         sliders
	 */
	protected JPanel getSettingsPanel()
	{
		final JPanel settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));

		numAgentsLabel = new JLabel("Number of Agents: " + numAgents);
		numAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numAgentsLabel);

		final Hashtable numAgentsLabels = new Hashtable();
		numAgentsLabels.put(new Integer(MIN_NUM_AGENTS), new JLabel(Integer.toString(MIN_NUM_AGENTS)));
		numAgentsLabels.put(new Integer(MAX_NUM_AGENTS), new JLabel(Integer.toString(MAX_NUM_AGENTS)));

		numAgentsSlider = new JSlider(MIN_NUM_AGENTS, MAX_NUM_AGENTS, numAgents);
		numAgentsSlider.setLabelTable(numAgentsLabels);
		numAgentsSlider.setPaintLabels(true);
		numAgentsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numAgentsChanged(event);
			}
		});
		settingsPanel.add(numAgentsSlider);

		agentSightLabel = new JLabel("Agent Sight: " + agentSight);
		agentSightLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(agentSightLabel);

		final Hashtable agentSightLabels = new Hashtable();
		agentSightLabels.put(new Integer(MIN_AGENT_SIGHT), new JLabel(Integer.toString(MIN_AGENT_SIGHT)));
		agentSightLabels.put(new Integer(MAX_AGENT_SIGHT), new JLabel(Integer.toString(MAX_AGENT_SIGHT)));

		agentSightSlider = new JSlider(MIN_AGENT_SIGHT, MAX_AGENT_SIGHT, agentSight);
		agentSightSlider.setLabelTable(agentSightLabels);
		agentSightSlider.setPaintLabels(true);
		agentSightSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				agentSightChanged(event);
			}
		});
		settingsPanel.add(agentSightSlider);

		accelerationFactorLabel = new JLabel("Acceleration Factor: " + PERCENT_FORMAT.format(accelerationFactor / 100.0));
		accelerationFactorLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(accelerationFactorLabel);

		final Hashtable accelerationFactorLabels = new Hashtable();
		accelerationFactorLabels.put(new Integer(MIN_ACCELERATION_FACTOR), new JLabel(PERCENT_FORMAT.format(MIN_ACCELERATION_FACTOR / 100.0)));
		accelerationFactorLabels.put(new Integer(MAX_ACCELERATION_FACTOR), new JLabel(PERCENT_FORMAT.format(MAX_ACCELERATION_FACTOR / 100.0)));

		accelerationFactorSlider = new JSlider(MIN_ACCELERATION_FACTOR, MAX_ACCELERATION_FACTOR, accelerationFactor);
		accelerationFactorSlider.setLabelTable(accelerationFactorLabels);
		accelerationFactorSlider.setPaintLabels(true);
		accelerationFactorSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				accelerationFactorChanged(event);
			}
		});
		settingsPanel.add(accelerationFactorSlider);

		stopWhenNoNeighborsCheckBox = new JCheckBox("Stop When No Visible Neighbors");
		stopWhenNoNeighborsCheckBox.setSelected(stopWhenNoNeighbors);
		stopWhenNoNeighborsCheckBox.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		stopWhenNoNeighborsCheckBox.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				stopWhenNoNeighborsClicked(event);
			}
		});
		settingsPanel.add(stopWhenNoNeighborsCheckBox);

		final JLabel changes = new JLabel("Changes will take");
		changes.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		final JLabel takeEffect = new JLabel("effect at next reset");
		takeEffect.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(changes);
		settingsPanel.add(takeEffect);
		return settingsPanel;
	}

	/**
	 * The method invoked when the "Number of Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numAgentsChanged(final ChangeEvent event)
	{
		numAgents = numAgentsSlider.getValue();
		numAgentsLabel.setText("Number of Agents: " + numAgents);
	}

	/**
	 * The method invoked when the "Agent Sight" slider changes
	 *
	 * @param event The change event
	 */
	public void agentSightChanged(final ChangeEvent event)
	{
		agentSight = agentSightSlider.getValue();
		agentSightLabel.setText("Agent Sight: " + agentSight);
	}

	/**
	 * The method invoked when the "Acceleration Factor" slider changes
	 *
	 * @param event The change event
	 */
	public void accelerationFactorChanged(final ChangeEvent event)
	{
		accelerationFactor = accelerationFactorSlider.getValue();
		accelerationFactorLabel.setText("Acceleration Factor: " + PERCENT_FORMAT.format(accelerationFactor / 100.0));
	}

	/**
	 * The method invoked when the user selects or deselects the "Choose Neighbors
	 * by Proximity" check box
	 *
	 * @param event The action event
	 */
	public void stopWhenNoNeighborsClicked(final ActionEvent event)
	{
		stopWhenNoNeighbors = stopWhenNoNeighborsCheckBox.isSelected();
	}
}