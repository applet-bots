package appletbots;

/**
 * A world listener
 *
 * @author $Author: michael $
 * @version $Revision: 1.1.1.1 $
 */
public interface WorldListener
{
	void threadStopped();

	void threadStarted();

	void timeIncremented(long time);
}
