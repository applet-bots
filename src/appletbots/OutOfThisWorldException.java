/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

/**
 * This exception is thrown when an object (or part of an object) leaves the
 * boundaries of the world.
 *
 * @author Erik Rasmussen
 */
public class OutOfThisWorldException extends Exception
{
	/**
	 * Creates a new OutOfTheWorldException with the given message
	 *
	 * @param message The error message
	 */
	public OutOfThisWorldException(final String message)
	{
		super(message);
	}

	/**
	 * Creates a new OutOfThisWorldException
	 */
	public OutOfThisWorldException()
	{
		super();
	}
}