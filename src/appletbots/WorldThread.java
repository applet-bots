/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

/**
 * This is the main thread that drives the changes in the world.
 *
 * @author Erik Rasmussen
 */
public class WorldThread extends Thread
{
	/**
	 * The world this WorldThread is running on
	 */
	private World world;
	/**
	 * Whether or not the thread is running or should continue running
	 */
	private boolean running;

	/**
	 * Creates a new world thread to run on the given world
	 *
	 * @param world The world the thread should run for
	 */
	public WorldThread(final World world)
	{
		this.world = world;
	}

	/**
	 * Calls world.incrementTime() and sleeps for world.getDelay()
	 */
	public void run()
	{
		running = true;
		while (running)
		{
			world.incrementTime();
			try
			{
				Thread.sleep(world.getDelay());
			}
			catch (InterruptedException e)
			{
				running = false;
			}
		}
	}

	/**
	 * Returns whether or not this world thread is running or should continue
	 * to run
	 *
	 * @return Whether or not this world thread is running or should continue
	 *         to run
	 */
	public boolean getRunning()
	{
		return running;
	}

	/**
	 * Sets whether or not this world thread is running or should continue to
	 * run
	 *
	 * @param running Whether or not this world thread is running or should
	 *                continue to run
	 */
	public void setRunning(final boolean running)
	{
		this.running = false;
	}
}