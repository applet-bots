/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.directedballs;

import appletbots.World;
import appletbots.balls.Ball;
import appletbots.geometry.Vector;
import appletbots.Applet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.Hashtable;

/**
 * An applet to run a DirectedKicker simulation
 *
 * @author Erik Rasmussen
 */
public class DirectedBallsApplet extends Applet
{
	/**
	 * The maximum number of red agents allowed
	 */
	public static final int MAX_RED_NUM_AGENTS = 50;
	/**
	 * The minimum number of red agents allowed
	 */
	public static final int MIN_RED_NUM_AGENTS = 1;
	/**
	 * The number of red agents
	 */
	protected int numRedAgents = 5;
	/**
	 * The "Number of Red Agents" slider
	 */
	protected JSlider numRedAgentsSlider;
	/**
	 * The "Number of Red Agents" label
	 */
	protected JLabel numRedAgentsLabel;
	/**
	 * The maximum number of blue agents allowed
	 */
	public static final int MAX_BLUE_NUM_AGENTS = 50;
	/**
	 * The minimum number of blue agents allowed
	 */
	public static final int MIN_BLUE_NUM_AGENTS = 1;
	/**
	 * The number of blue agents
	 */
	protected int numBlueAgents = 5;
	/**
	 * The "Number of Blue Agents" slider
	 */
	protected JSlider numBlueAgentsSlider;
	/**
	 * The "Number of Blue Agents" label
	 */
	protected JLabel numBlueAgentsLabel;
	/**
	 * The maximum agent sight value allowed
	 */
	public static final int MAX_AGENT_SIGHT = 100;
	/**
	 * The minimum agent sight value allowed
	 */
	public static final int MIN_AGENT_SIGHT = 5;
	/**
	 * The agent sight value
	 */
	protected int agentSight = 60;
	/**
	 * The "Agent Sight" slider
	 */
	protected JSlider agentSightSlider;
	/**
	 * The "Agent Sight" label
	 */
	protected JLabel agentSightLabel;
	/**
	 * The maximum number of balls allowed
	 */
	public static final int MAX_NUM_BALLS = 5;
	/**
	 * The minimum number of balls allowed
	 */
	public static final int MIN_NUM_BALLS = 1;
	/**
	 * The number of balls
	 */
	protected int numBalls = 2;
	/**
	 * The "Number of Balls" slider
	 */
	protected JSlider numBallsSlider;
	/**
	 * The "Number of Balls" label
	 */
	protected JLabel numBallsLabel;
	/**
	 * The maximum allowed ball mass
	 */
	public static final int MAX_BALL_MASS = 100;
	/**
	 * The minimum allowed ball mass
	 */
	public static final int MIN_BALL_MASS = 5;
	/**
	 * The ball mass
	 */
	protected int ballMass = 10;
	/**
	 * The "Ball Mass" slider
	 */
	protected JSlider ballMassSlider;
	/**
	 * The "Ball Mass" label
	 */
	protected JLabel ballMassLabel;

	/**
	 * Initializes the world with the appropriate number of balls and
	 * DirectedKickerAgents with the appropriate "agent sight" settings
	 *
	 * @return A world with DirectedKickerAgents
	 */
	protected World initializeWorld()
	{
		final World world = new World(300, 300);
		try
		{
			for (int i = 0; i < numBalls; i++)
				world.addObject(new Ball(10, ballMass, Color.red), Vector.getRandom(5));
			for (int i = 0; i < numRedAgents; i++)
				world.addObject(new DirectedKicker(new Vector(0, 1), Color.pink, 5, agentSight, 5, 3));
			for (int i = 0; i < numBlueAgents; i++)
				world.addObject(new DirectedKicker(new Vector(0, -1), Color.cyan, 5, agentSight, 5, 3));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return world;
	}

	/**
	 * Returns a settings panel with "Number of Red Agents", "Number of Blue
	 * Agents", "Number of Balls", "Agent Sight", and "Ball Mass" sliders
	 *
	 * @return A settings panel with "Number of Red Agents", "Number of Blue
	 *         Agents", "Number of Balls", "Agent Sight", and "Ball Mass"
	 *         sliders
	 */
	protected JPanel getSettingsPanel()
	{
		final JPanel settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));

		numRedAgentsLabel = new JLabel("Number of Red Agents: " + numRedAgents);
		numRedAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numRedAgentsLabel);

		final Hashtable numRedAgentsLabels = new Hashtable();
		numRedAgentsLabels.put(new Integer(MIN_RED_NUM_AGENTS), new JLabel(Integer.toString(MIN_RED_NUM_AGENTS)));
		numRedAgentsLabels.put(new Integer(MAX_RED_NUM_AGENTS), new JLabel(Integer.toString(MAX_RED_NUM_AGENTS)));

		numRedAgentsSlider = new JSlider(MIN_RED_NUM_AGENTS, MAX_RED_NUM_AGENTS, numRedAgents);
		numRedAgentsSlider.setLabelTable(numRedAgentsLabels);
		numRedAgentsSlider.setPaintLabels(true);
		numRedAgentsSlider.addChangeListener(new javax.swing.event.ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numRedAgentsChanged(event);
			}
		});
		settingsPanel.add(numRedAgentsSlider);

		numBlueAgentsLabel = new JLabel("Number of Blue Agents: " + numBlueAgents);
		numBlueAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numBlueAgentsLabel);

		final Hashtable numBlueAgentsLabels = new Hashtable();
		numBlueAgentsLabels.put(new Integer(MIN_BLUE_NUM_AGENTS), new JLabel(Integer.toString(MIN_BLUE_NUM_AGENTS)));
		numBlueAgentsLabels.put(new Integer(MAX_BLUE_NUM_AGENTS), new JLabel(Integer.toString(MAX_BLUE_NUM_AGENTS)));

		numBlueAgentsSlider = new JSlider(MIN_BLUE_NUM_AGENTS, MAX_BLUE_NUM_AGENTS, numBlueAgents);
		numBlueAgentsSlider.setLabelTable(numBlueAgentsLabels);
		numBlueAgentsSlider.setPaintLabels(true);
		numBlueAgentsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numBlueAgentsChanged(event);
			}
		});
		settingsPanel.add(numBlueAgentsSlider);

		agentSightLabel = new JLabel("Agent Sight: " + agentSight);
		agentSightLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(agentSightLabel);

		final Hashtable agentSightLabels = new Hashtable();
		agentSightLabels.put(new Integer(MIN_AGENT_SIGHT), new JLabel(Integer.toString(MIN_AGENT_SIGHT)));
		agentSightLabels.put(new Integer(MAX_AGENT_SIGHT), new JLabel(Integer.toString(MAX_AGENT_SIGHT)));

		agentSightSlider = new JSlider(MIN_AGENT_SIGHT, MAX_AGENT_SIGHT, agentSight);
		agentSightSlider.setLabelTable(agentSightLabels);
		agentSightSlider.setPaintLabels(true);
		agentSightSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				agentSightChanged(event);
			}
		});
		settingsPanel.add(agentSightSlider);

		numBallsLabel = new JLabel("Number of Balls: " + numBalls);
		numBallsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numBallsLabel);

		final Hashtable numBallsLabels = new Hashtable();
		numBallsLabels.put(new Integer(MIN_NUM_BALLS), new JLabel(Integer.toString(MIN_NUM_BALLS)));
		numBallsLabels.put(new Integer(MAX_NUM_BALLS), new JLabel(Integer.toString(MAX_NUM_BALLS)));

		numBallsSlider = new JSlider(MIN_NUM_BALLS, MAX_NUM_BALLS, numBalls);
		numBallsSlider.setLabelTable(numBallsLabels);
		numBallsSlider.setPaintLabels(true);
		numBallsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numBallsChanged(event);
			}
		});
		settingsPanel.add(numBallsSlider);

		ballMassLabel = new JLabel("Ball Mass: " + ballMass);
		ballMassLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(ballMassLabel);

		final Hashtable ballMassLabels = new Hashtable();
		ballMassLabels.put(new Integer(MIN_BALL_MASS), new JLabel(Integer.toString(MIN_BALL_MASS)));
		ballMassLabels.put(new Integer(MAX_BALL_MASS), new JLabel(Integer.toString(MAX_BALL_MASS)));

		ballMassSlider = new JSlider(MIN_BALL_MASS, MAX_BALL_MASS, ballMass);
		ballMassSlider.setLabelTable(ballMassLabels);
		ballMassSlider.setPaintLabels(true);
		ballMassSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				ballMassChanged(event);
			}
		});
		settingsPanel.add(ballMassSlider);

		final JLabel changes = new JLabel("Changes will take");
		changes.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		final JLabel takeEffect = new JLabel("effect at next reset");
		takeEffect.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(changes);
		settingsPanel.add(takeEffect);
		return settingsPanel;
	}

	/**
	 * The method invoked when the "Number of Red Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numRedAgentsChanged(final ChangeEvent event)
	{
		numRedAgents = numRedAgentsSlider.getValue();
		numRedAgentsLabel.setText("Number of Red Agents: " + numRedAgents);
	}

	/**
	 * The method invoked when the "Number of Blue Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numBlueAgentsChanged(final ChangeEvent event)
	{
		numBlueAgents = numBlueAgentsSlider.getValue();
		numBlueAgentsLabel.setText("Number of Blue Agents: " + numBlueAgents);
	}

	/**
	 * The method invoked when the "Agent Sight" slider changes
	 *
	 * @param event The change event
	 */
	public void agentSightChanged(final ChangeEvent event)
	{
		agentSight = agentSightSlider.getValue();
		agentSightLabel.setText("Agent Sight: " + agentSight);
	}

	/**
	 * The method invoked when the "Number of Balls" slider changes
	 *
	 * @param event The change event
	 */
	public void numBallsChanged(final ChangeEvent event)
	{
		numBalls = numBallsSlider.getValue();
		numBallsLabel.setText("Number of Balls: " + numBalls);
	}

	/**
	 * The method invoked when the "Ball Mass" slider changes
	 *
	 * @param event The change event
	 */
	public void ballMassChanged(final ChangeEvent event)
	{
		ballMass = ballMassSlider.getValue();
		ballMassLabel.setText("Ball Mass: " + ballMass);
	}
}