/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.directedballs;

import appletbots.balls.Ball;
import appletbots.balls.Kicker;
import appletbots.geometry.Vector;

import java.awt.*;

/**
 * A directed kicker agent chooses a visible ball at random and tries to "kick"
 * it so that the ball will move in the the bot's goal direction.  The agent
 * calculates the spot on the ball it needs to hit to knock the ball in the
 * right direction.  If it senses that moving directly towards the target spot
 * will result in kicking the ball in another direction, the agent accelerates
 * around the bot (orthagonal to the vector towards the target spot) until it
 * can hit the target spot.
 *
 * @author Erik Rasmussen
 */
public class DirectedKicker extends Kicker
{
	/**
	 * The goal direction
	 */
	protected Vector direction;

	/**
	 * Constructs a new DirectedKicker with the given parameters
	 *
	 * @param direction       The agent's goal direction
	 * @param color           The agent's color
	 * @param size            The agent's radius
	 * @param sight           The distance the agent can see
	 * @param maxSpeed        The maximum speed the agent can travel
	 * @param maxAcceleration The maximum acceleration for this agent
	 */
	public DirectedKicker(final Vector direction, final Color color, final int size, final int sight, final double maxSpeed, final double maxAcceleration)
	{
		super(size, sight, maxSpeed, maxAcceleration);
		this.direction = direction;
		this.color = color;
		direction.normalize();
	}

	/**
	 * Observes the world, and follows the Directed Kicker Algorithm.
	 */
	public void observeWorld()
	{
		final Ball ball = chooseBall();
		if (ball != null)
		{
			Vector toBallCenter = world.getVectorToObject(this, ball);
			toBallCenter = toBallCenter.setLength(toBallCenter.getLength() + ball.getSize());
			Vector fromCenterOfBallToContactPoint = direction.multiply(-1);
			fromCenterOfBallToContactPoint = fromCenterOfBallToContactPoint.setLength(ball.getSize() + getSize());
			final Vector toContactPoint = toBallCenter.add(fromCenterOfBallToContactPoint);

			if (toContactPoint.dotProduct(fromCenterOfBallToContactPoint) > 0)
			{
				// there's not a straight path to the contact point
				// we need to go around the ball and hit it from the other side
				Vector orthagToContactPoint = new Vector(toContactPoint.y, toContactPoint.x);
				if (orthagToContactPoint.dotProduct(direction.multiply(-1)) < 0)
					orthagToContactPoint = orthagToContactPoint.multiply(-1);
				setAcceleration(orthagToContactPoint);
			}
			else
				setAcceleration(toBallCenter);
		}
		else
		{
			// continue in straight path until we find a ball to kick
			accelerationVectorColor = null;
			setAcceleration(new Vector(0, 0));
		}
	}
}