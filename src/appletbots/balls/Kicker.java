/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.balls;

import appletbots.Agent;
import appletbots.WorldObject;
import appletbots.geometry.Vector;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * A kicker agent chooses a visible ball at random and accelerates towards it.
 * If no ball is visible the agent sets his acceleration to zero.
 *
 * @author Erik Rasmussen
 */
public class Kicker extends Agent
{
	/**
	 * Constructs a new Kicker with the given parameters
	 *
	 * @param size            The agent's radius
	 * @param sight           The distance the agent can see
	 * @param maxSpeed        The maximum speed the agent can travel
	 * @param maxAcceleration The maximum acceleration for this agent
	 */
	public Kicker(final int size, final int sight, final double maxSpeed, final double maxAcceleration)
	{
		super(size, sight, maxSpeed, maxAcceleration);
		this.color = Color.cyan;
	}

	/**
	 * Observes the world, and follows the Kicker Algorithm.
	 */
	public void observeWorld()
	{
		final Ball ball = chooseBall();
		if (ball != null)
		{
			setAcceleration(world.getVectorToObject(this, ball));
		}
		else
		{
			// continue in straight path until we find a ball to kick
			accelerationVectorColor = null;
			setAcceleration(new Vector(0, 0));
		}
	}

	/**
	 * Selects a ball at random from those visible
	 *
	 * @return The chosen ball
	 */
	protected Ball chooseBall()
	{
		final Collection seenObjects = world.getSeenObjects(this);
		final List balls = new ArrayList();
		for (Iterator iterator = seenObjects.iterator(); iterator.hasNext();)
		{
			final WorldObject object = (WorldObject) iterator.next();
			if (object instanceof Ball)
				balls.add(object);
		}
		if (!balls.isEmpty())
			return (Ball) balls.get((int) Math.floor(Math.random() * balls.size()));
		else
			return null;
	}
}