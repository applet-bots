/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.balls;

import appletbots.WorldObject;

import java.awt.*;

/**
 * The class represents a ball in the appletbots world
 *
 * @author Erik Rasmussen
 */
public class Ball extends WorldObject
{
	/**
	 * Constructs a ball with the given size (radius) and color.  The ball's
	 * mass is set to the same as the ball's size, and the maxSpeed is set to
	 * twice the size (the diameter).
	 *
	 * @param size  The radius of the ball
	 * @param color The color of the ball
	 */
	public Ball(final int size, final Color color)
	{
		this(size, (double) size, color);
	}

	/**
	 * Constructs a ball with the given size (radius), mass, and color.  The
	 * ball's maxSpeed is set to twice the size (the diameter).
	 *
	 * @param size  The radius of the ball
	 * @param mass  The mass of the ball
	 * @param color The color of the ball
	 */
	public Ball(final int size, final double mass, final Color color)
	{
		this(size, size / 2.0, mass, color);
	}

	/**
	 * Constructs a ball with the given size (radius), maxSpeed, mass, and
	 * color.
	 *
	 * @param size     The radius of the ball
	 * @param maxSpeed The maximum speed of the ball
	 * @param mass     The mass of the ball
	 * @param color    The color of the ball
	 */
	public Ball(final int size, final double maxSpeed, final double mass, final Color color)
	{
		super(mass, maxSpeed, size, color);
	}
}