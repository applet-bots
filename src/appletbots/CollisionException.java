/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

/**
 * This exception is thrown when two objects collide
 *
 * @author Erik Rasmussen
 */
public class CollisionException extends Exception
{
	/**
	 * The collider.  The object that is colliding with the collidee.
	 */
	private WorldObject collider;
	/**
	 * The collidee.  The object that the collider is colliding with.
	 */
	private WorldObject collidee;

	/**
	 * Creates a new CollisionException with the given collider and collidee
	 *
	 * @param collider The collider.  The object that is colliding with the collidee.
	 * @param collidee The collidee.  The object that the collider is colliding with.
	 */
	public CollisionException(final WorldObject collider, final WorldObject collidee)
	{
		super();
		this.collider = collider;
		this.collidee = collidee;
	}

	/**
	 * Creates a new CollisionException with the given error message
	 *
	 * @param message The error message
	 */
	public CollisionException(final String message)
	{
		super(message);
	}

	/**
	 * Creates a new CollisionException
	 */
	public CollisionException()
	{
		super();
	}

	/**
	 * Returns the collider.  The object that is colliding with the collidee.
	 *
	 * @return The collider.  The object that is colliding with the collidee.
	 */
	public WorldObject getCollider()
	{
		return collider;
	}

	/**
	 * Returns the collidee.  The object that the collider is colliding with.
	 *
	 * @return The collidee.  The object that the collider is colliding with.
	 */
	public WorldObject getCollidee()
	{
		return collidee;
	}
}