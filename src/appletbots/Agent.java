/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

import appletbots.geometry.Vector;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents an Agent in a appletbots world.
 *
 * @author Erik Rasmussen
 */
public abstract class Agent extends WorldObject
{
	/**
	 * The distance the agent can see
	 */
	protected int sight;
	/**
	 * The maximum amount of acceleration for this agent
	 */
	protected double maxAcceleration;
	/**
	 * The world in which the agent exists
	 */
	protected World world;
	/**
	 * The agent's acceleration
	 */
	private Vector acceleration = new Vector(0, 0);

	/**
	 * The color to paint the velocity vector
	 */
	protected Color velocityVectorColor;
	/**
	 * The color to paint the acceleration vector
	 */
	protected Color accelerationVectorColor;

	/**
	 * Constructs a new agent with the following default values:<br>
	 * size = 5<br>
	 * sight = 60<br>
	 * maxSpeed = 5.0<br>
	 * maxAcceleration = 3.0<br>
	 */
	public Agent()
	{
		this(5, 60, 5, 3);
	}

	/**
	 * Constructs an agent with the given parameters
	 *
	 * @param size            The agent's radius
	 * @param sight           The distance the agent can see
	 * @param maxSpeed        The maximum speed the agent can travel
	 * @param maxAcceleration The maximum acceleration for this agent
	 */
	public Agent(final int size, final int sight, final double maxSpeed, final double maxAcceleration)
	{
		super(size, maxSpeed, size, Color.white);
		this.sight = sight;
		this.maxAcceleration = maxAcceleration;
	}

	/**
	 * Returns the distance this agent can see
	 *
	 * @return The distance this agent can see
	 */
	public int getSight()
	{
		return sight;
	}

	/**
	 * Tells the agent what world he is in.  This is called by
	 * World.addAgent().
	 *
	 * @param world The world the agent exists in
	 */
	public void setWorld(final World world)
	{
		this.world = world;
	}

	/**
	 * Returns a random acceleration vector
	 *
	 * @return A random acceleration vector
	 */
	public final Vector getRandomAcceleration()
	{
		return Vector.getRandom(Math.random() * maxAcceleration);
	}

	/**
	 * Returns the agent's acceleration.  Note that the vector returned is a
	 * clone of the internal object, so changing the returned vector will not
	 * affect the agent's acceleration.  To modify the agent's acceleration,
	 * use setAcceleration(Vector).
	 *
	 * @return The agent's acceleration
	 */
	public Vector getAcceleration()
	{
		return (Vector) acceleration.clone();
	}

	/**
	 * Sets the agent's acceleration vector.  If the given vector has a
	 * magnitude greater than the agent's maxAcceleration, then the vector is
	 * scaled down to the maxAcceleration.
	 *
	 * @param acceleration The agent's acceleration
	 */
	protected void setAcceleration(final Vector acceleration)
	{
		this.acceleration = (Vector) acceleration.clone();
		if (this.acceleration.getLength() > maxAcceleration)
			this.acceleration = this.acceleration.setLength(maxAcceleration);
	}

	/**
	 * Returns the agent's maximum acceleration
	 *
	 * @return The agent's maximum acceleration
	 */
	public double getMaxAcceleration()
	{
		return maxAcceleration;
	}

	/**
	 * Returns an array of vectors that should be drawn at the time this agent
	 * is painted
	 *
	 * @return An array of vectors that should be drawn at the time this agent
	 *         is painted
	 */
	public VectorToDraw[] getVectorsToDraw()
	{
		final List vectorsToDraw = new ArrayList();
		if (accelerationVectorColor != null)
			vectorsToDraw.add(new VectorToDraw(getAcceleration().multiply(size), accelerationVectorColor));
		if (velocityVectorColor != null)
			vectorsToDraw.add(new VectorToDraw(world.getVelocity(this).multiply(size), velocityVectorColor));
		final VectorToDraw[] vectorsToDrawArray = new VectorToDraw[vectorsToDraw.size()];
		for (int i = 0; i < vectorsToDraw.size(); i++)
			vectorsToDrawArray[i] = (VectorToDraw) vectorsToDraw.get(i);
		return vectorsToDrawArray;
	}

	/**
	 * Sets a flag to let the agent know whether or not to draw his
	 * acceleration vector
	 *
	 * @param showAcceleration Whether or not to draw the acceleration vector
	 */
	public void setShowAcceleration(final boolean showAcceleration)
	{
		accelerationVectorColor = showAcceleration ? Color.magenta : null;
	}

	/**
	 * Returns whether or not the agent's acceleration vector will be drawn
	 *
	 * @return Whether or not the agent's acceleration vector will be drawn
	 */
	public boolean getShowAcceleration()
	{
		return accelerationVectorColor != null;
	}

	/**
	 * Sets a flag to let the agent know whether or not to draw his velocity
	 * vector
	 *
	 * @param showVelocity Whether or not to draw the velocity vector
	 */
	public void setShowVelocity(final boolean showVelocity)
	{
		velocityVectorColor = showVelocity ? Color.orange : null;
	}

	/**
	 * Returns whether or not the agent's velocity vector will be drawn
	 *
	 * @return Whether or not the agent's velocity vector will be drawn
	 */
	public boolean getShowVelocity()
	{
		return velocityVectorColor != null;
	}

	/**
	 * The method invoked to allow the agent to observe the world and
	 * optionally modify his acceleration to try to achieve a goal.
	 */
	public abstract void observeWorld();
}