/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.friendly;

import appletbots.Agent;
import appletbots.geometry.Vector;

import java.awt.*;
import java.util.List;

/**
 * A friendly agent looks at all the other agents he can see and randomly
 * selects one to be his friend.  However, he can only make another agent his
 * friend if the other agent does not already have the agent selected as his
 * friend.  The friendly agent then tries to move toward his friend by
 * accelerating towards him.  The agent remembers who his friend is and
 * continues to accelerate towards him as long as the friend remains visible.
 * If the agent ever loses sight of his friend he simply selects another one at
 * random given the selection criteria stated above.  If ever a friend cannot
 * be found the agent accelerates in a random direction to search for one.
 *
 * @author Erik Rasmussen
 */
public class FriendlyAgent extends Agent
{
	/**
	 * The selected friend
	 */
	private Agent friend;

	/**
	 * Constructs a new FriendlyAgent with the given parameters
	 *
	 * @param size            The agent's radius
	 * @param sight           The distance the agent can see
	 * @param maxSpeed        The maximum speed the agent can travel
	 * @param maxAcceleration The maximum acceleration for this agent
	 */
	public FriendlyAgent(final int size, final int sight, final double maxSpeed, final double maxAcceleration)
	{
		super(size, sight, maxSpeed, maxAcceleration);
		this.color = Color.cyan;
	}

	/**
	 * Observes the world, and follows the Friendly Agent Algorithm.
	 */
	public void observeWorld()
	{
		final List neighbors = world.getNeighbors(this);
		if (neighbors.isEmpty())
			friend = null;
		else
		{
			if (friend == null || !neighbors.contains(friend))
			{
				// remove neighbors that have me as their friend
				for (int i = 0; i < neighbors.size(); i++)
				{
					final Agent neighbor = (Agent) neighbors.get(i);
					if (neighbor instanceof FriendlyAgent)
					{
						final FriendlyAgent friendlyNeighbor = (FriendlyAgent) neighbor;
						if (equals(friendlyNeighbor.getFriend()))
							neighbors.remove(i--);
					}
				}
				if (neighbors.isEmpty())
					friend = null;
				else
				{
					// can't follow old one, pick a random neighbor and go to him
					final int randIndex = (int) Math.floor(Math.random() * neighbors.size());
					friend = (Agent) neighbors.get(randIndex);
				}
			}
		}
		if (friend != null)
			setAcceleration(world.getVectorToObject(this, friend));
		else
			setAcceleration(Vector.getRandom(maxAcceleration));
	}

	/**
	 * Returns the selected friend
	 *
	 * @return The selected friend
	 */
	public Agent getFriend()
	{
		return friend;
	}
}