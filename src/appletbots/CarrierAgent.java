/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserved
 */
package appletbots;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents an agent that can pick up objects in the
 * appletbots world.
 *
 * @author Erik Rasmussen
 */
public abstract class CarrierAgent extends Agent
{
	/**
	 * The list of objects the agent is carrying
	 */
	private List inventory = new ArrayList();
	/**
	 * The distance the agents needs to be from an object to be able to pick
	 * it up
	 */
	private int pickUpDistance;

	/**
	 * Constructs an agent with the given parameters
	 *
	 * @param pickUpDistance  How close a CarrierAgent must be to an object to
	 *                        pick it up
	 * @param size            The agent's radius
	 * @param sight           The distance the agent can see
	 * @param maxSpeed        The maximum speed the agent can travel
	 * @param maxAcceleration The maximum acceleration for this agent
	 */
	public CarrierAgent(final int pickUpDistance, final int size, final int sight, final double maxSpeed, final double maxAcceleration)
	{
		super(size, sight, maxSpeed, maxAcceleration);
		this.pickUpDistance = pickUpDistance;
	}

	/**
	 * Picks up an object
	 *
	 * @param item The object to pick up
	 */
	protected void pickUp(final CarriableObject item)
	{
		inventory.add(item);
		item.setCarriedBy(this);
	}

	/**
	 * Drops an object
	 *
	 * @param item The object to drop
	 * @throws CollisionException Thrown if the item cannot be dropped within
	 *                            the "pickup distance"
	 */
	protected void drop(final CarriableObject item) throws CollisionException
	{
		item.setCarriedBy(null);
		try
		{
			world.dropItem(this, item);
			inventory.remove(item);
		}
		catch (CollisionException e)
		{
			item.setCarriedBy(this);
			throw e;
		}
	}

	/**
	 * Returns whether or not the agent is carrying the given item
	 *
	 * @param item The item to check for
	 * @return Whether or not the agent is carrying the given item
	 */
	public boolean hasItem(final CarriableObject item)
	{
		return inventory.contains(item);
	}

	/**
	 * Returns whether or not the agent is carrying any items
	 *
	 * @return Whether or not the agent is carrying any items
	 */
	public boolean hasItems()
	{
		return !inventory.isEmpty();
	}

	/**
	 * Returns the contents of the agent's inventory
	 *
	 * @return The contents of the agent's inventory
	 */
	public final CarriableObject[] getInventory()
	{
		synchronized (inventory)
		{
			if (inventory.isEmpty())
				return new CarriableObject[]{};
			final CarriableObject[] items = new CarriableObject[inventory.size()];
			for (int i = 0; i < inventory.size(); i++)
				items[i] = (CarriableObject) inventory.get(i);
			return items;
		}
	}

	/**
	 * Returns the distance the agents needs to be from an object to be able
	 * to pick it up
	 *
	 * @return The distance the agents needs to be from an object to be able
	 *         to pick it up
	 */
	public int getPickUpDistance()
	{
		return pickUpDistance;
	}
}