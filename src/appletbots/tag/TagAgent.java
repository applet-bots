/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.tag;

import appletbots.Agent;
import appletbots.WorldObject;
import appletbots.geometry.Vector;

import java.awt.*;
import java.util.List;

/**
 * If a tag agent is "it", he will try to touch whichever non-"it" agent is
 * closest to him.  When that contact is made, the agent he touched is "it".
 * If a tag agent is not "it", he will not accelerate unless he sees an agent
 * that is "it", in which case the non-"it" agent will accelerate away from
 * the agent that is "it".  Additionally, when an agent is tagged "it", its
 * maximum speed decreases by 20% and it must wait 10 time cycles before
 * accelerating; this is to prevent two agents getting locked together
 * repeatedly tagging one another.
 *
 * @author Erik Rasmussen
 */
public class TagAgent extends Agent
{
	/**
	 * The "it" flag
	 */
	private boolean it;
	/**
	 * The number of time cycles to wait after being tagged "it"
	 */
	private int timeToWait;

	/**
	 * Constructs a new TagAgent with the given parameters
	 *
	 * @param size            The agent's radius
	 * @param sight           The distance the agent can see
	 * @param maxSpeed        The maximum speed the agent can travel
	 * @param maxAcceleration The maximum acceleration for this agent
	 */
	public TagAgent(final int size, final int sight, final double maxSpeed, final double maxAcceleration)
	{
		super(size, sight, maxSpeed, maxAcceleration);
		this.color = Color.cyan;
	}

	/**
	 * Observes the world, and follows the Tag Agent Algorithm.
	 */
	public void observeWorld()
	{
		if (timeToWait > 0)
		{
			setAcceleration(new Vector(0, 0));
			timeToWait--;
			return;
		}
		final List neighbors = world.getNeighbors(this);
		double distanceToClosestOppNeighbor = Double.MAX_VALUE;
		TagAgent closestNeighborWithOppositeItFlag = null;
		for (int i = 0; i < neighbors.size(); i++)
		{
			final Agent neighbor = (Agent) neighbors.get(i);
			if (neighbor instanceof TagAgent)
			{
				final TagAgent tagNeighbor = (TagAgent) neighbor;
				if (tagNeighbor.isIt() == !it)
				{
					final double distance = world.getVectorToObject(this, tagNeighbor).getLength();
					if (distance < distanceToClosestOppNeighbor)
					{
						distanceToClosestOppNeighbor = distance;
						closestNeighborWithOppositeItFlag = tagNeighbor;
					}
				}
			}
		}
		if (closestNeighborWithOppositeItFlag == null)
			setAcceleration(new Vector(0, 0));
		else
		{
			Vector toClosestOppNeighbor = world.getVectorToObject(this, closestNeighborWithOppositeItFlag);
			toClosestOppNeighbor = toClosestOppNeighbor.setLength(maxAcceleration);
			setAcceleration(it ? toClosestOppNeighbor : toClosestOppNeighbor.multiply(-1));
		}
	}

	/**
	 * Sets whether or not this agent is "it"
	 *
	 * @param it Whether or not this agent is "it"
	 */
	public void setIt(final boolean it)
	{
		if (this.it == !it)
		{
			this.it = it;
			if (it)
			{
				color = Color.pink;
				maxSpeed *= 0.8;
				timeToWait = 10;
			}
			else
			{
				color = Color.cyan;
				maxSpeed *= 1.25;
				timeToWait = 0;
			}
		}
	}

	/**
	 * Returns whether or not this agent is "it"
	 *
	 * @return Whether or not this agent is "it"
	 */
	public boolean isIt()
	{
		return it;
	}

	/**
	 * Informs the object that it has been in a collision
	 *
	 * @param object The object collided with
	 */
	public void collidedWith(final WorldObject object)
	{
		if (object instanceof TagAgent)
		{
			final TagAgent tagAgent = (TagAgent) object;
			if (tagAgent.isIt() == !it)
			{
				tagAgent.setIt(it);
				setIt(!it);
			}
		}
	}
}