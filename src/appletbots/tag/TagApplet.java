/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.tag;

import appletbots.World;
import appletbots.Applet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.Hashtable;

/**
 * An applet to run a TagAgent simulation
 *
 * @author Erik Rasmussen
 */
public class TagApplet extends Applet
{
	/**
	 * The maximum number of agents allowed
	 */
	public static final int MAX_NUM_AGENTS = 100;
	/**
	 * The minimum number of agents allowed
	 */
	public static final int MIN_NUM_AGENTS = 2;
	/**
	 * The number of agents
	 */
	protected int numAgents = 50;
	/**
	 * The "Number of Agents" slider
	 */
	protected JSlider numAgentsSlider;
	/**
	 * The "Number of Agents" label
	 */
	protected JLabel numAgentsLabel;
	/**
	 * The maximum number of "it" agents allowed
	 */
	public static final int MAX_NUM_IT_AGENTS = 20;
	/**
	 * The minimum number of "it" agents allowed
	 */
	public static final int MIN_NUM_IT_AGENTS = 1;
	/**
	 * The number of "it" agents
	 */
	protected int numItAgents = 1;
	/**
	 * The "Number of 'It' Agents" slider
	 */
	protected JSlider numItAgentsSlider;
	/**
	 * The "Number of 'It' Agents" label
	 */
	protected JLabel numItAgentsLabel;
	/**
	 * The maximum agent sight value allowed
	 */
	public static final int MAX_AGENT_SIGHT = 100;
	/**
	 * The minimum agent sight value allowed
	 */
	public static final int MIN_AGENT_SIGHT = 5;
	/**
	 * The agent sight value
	 */
	protected int agentSight = 60;
	/**
	 * The "Agent Sight" slider
	 */
	protected JSlider agentSightSlider;
	/**
	 * The "Agent Sight" label
	 */
	protected JLabel agentSightLabel;

	/**
	 * Initializes the world with the appropriate number of TagAgents
	 * with the appropriate "agent sight" settings
	 *
	 * @return A world with TagAgents
	 */
	protected World initializeWorld()
	{
		final World world = new World(300, 300);
		try
		{
			for (int i = 0; i < numAgents; i++)
				world.addObject(new TagAgent(5, agentSight, 5, 3));
			for (int i = 0; i < numItAgents; i++)
			{
				final TagAgent it = new TagAgent(5, agentSight, 5, 3);
				it.setIt(true);
				world.addObject(it);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return world;
	}

	/**
	 * Returns a settings panel with "Number of Agents", "Number of 'It'
	 * Agents", and "Agent Sight" sliders
	 *
	 * @return A settings panel with "Number of Agents", "Number of 'It'
	 *         Agents", and "Agent Sight" sliders
	 */
	protected JPanel getSettingsPanel()
	{
		final JPanel settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));

		numAgentsLabel = new JLabel("Number of Agents: " + numAgents);
		numAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numAgentsLabel);

		final Hashtable numAgentsLabels = new Hashtable();
		numAgentsLabels.put(new Integer(MIN_NUM_AGENTS), new JLabel(Integer.toString(MIN_NUM_AGENTS)));
		numAgentsLabels.put(new Integer(MAX_NUM_AGENTS), new JLabel(Integer.toString(MAX_NUM_AGENTS)));

		numAgentsSlider = new JSlider(MIN_NUM_AGENTS, MAX_NUM_AGENTS, numAgents);
		numAgentsSlider.setLabelTable(numAgentsLabels);
		numAgentsSlider.setPaintLabels(true);
		numAgentsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numAgentsChanged(event);
			}
		});
		settingsPanel.add(numAgentsSlider);

		numItAgentsLabel = new JLabel("Number of \"It\" Agents: " + numItAgents);
		numItAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numItAgentsLabel);

		final Hashtable numItAgentsLabels = new Hashtable();
		numItAgentsLabels.put(new Integer(MIN_NUM_IT_AGENTS), new JLabel(Integer.toString(MIN_NUM_IT_AGENTS)));
		numItAgentsLabels.put(new Integer(MAX_NUM_IT_AGENTS), new JLabel(Integer.toString(MAX_NUM_IT_AGENTS)));

		numItAgentsSlider = new JSlider(MIN_NUM_IT_AGENTS, MAX_NUM_IT_AGENTS, numItAgents);
		numItAgentsSlider.setLabelTable(numItAgentsLabels);
		numItAgentsSlider.setPaintLabels(true);
		numItAgentsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numItAgentsChanged(event);
			}
		});
		settingsPanel.add(numItAgentsSlider);

		agentSightLabel = new JLabel("Agent Sight: " + agentSight);
		agentSightLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(agentSightLabel);

		final Hashtable agentSightLabels = new Hashtable();
		agentSightLabels.put(new Integer(MIN_AGENT_SIGHT), new JLabel(Integer.toString(MIN_AGENT_SIGHT)));
		agentSightLabels.put(new Integer(MAX_AGENT_SIGHT), new JLabel(Integer.toString(MAX_AGENT_SIGHT)));

		agentSightSlider = new JSlider(MIN_AGENT_SIGHT, MAX_AGENT_SIGHT, agentSight);
		agentSightSlider.setLabelTable(agentSightLabels);
		agentSightSlider.setPaintLabels(true);
		agentSightSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				agentSightChanged(event);
			}
		});
		settingsPanel.add(agentSightSlider);

		final JLabel changes = new JLabel("Changes will take");
		changes.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		final JLabel takeEffect = new JLabel("effect at next reset");
		takeEffect.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(changes);
		settingsPanel.add(takeEffect);
		return settingsPanel;
	}

	/**
	 * The method invoked when the "Number of Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numAgentsChanged(final ChangeEvent event)
	{
		numAgents = numAgentsSlider.getValue();
		numAgentsLabel.setText("Number of Agents: " + numAgents);
	}

	/**
	 * The method invoked when the "Number of 'It' Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numItAgentsChanged(final ChangeEvent event)
	{
		numItAgents = numItAgentsSlider.getValue();
		numItAgentsLabel.setText("Number of \"It\" Agents: " + numItAgents);
	}

	/**
	 * The method invoked when the "Agent Sight" slider changes
	 *
	 * @param event The change event
	 */
	public void agentSightChanged(final ChangeEvent event)
	{
		agentSight = agentSightSlider.getValue();
		agentSightLabel.setText("Agent Sight: " + agentSight);
	}
}