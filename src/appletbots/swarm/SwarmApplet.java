/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.swarm;

import appletbots.World;
import appletbots.Applet;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;
import java.util.Hashtable;

/**
 * An applet to run a SwarmAgent simulation
 *
 * @author Erik Rasmussen
 */
public class SwarmApplet extends Applet
{
	/**
	 * The maximum number of agents allowed
	 */
	public static final int MAX_NUM_AGENTS = 100;
	/**
	 * The minimum number of agents allowed
	 */
	public static final int MIN_NUM_AGENTS = 2;
	/**
	 * The number of agents
	 */
	protected int numAgents = 50;
	/**
	 * The "Number of Agents" slider
	 */
	protected JSlider numAgentsSlider;
	/**
	 * The "Number of Agents" label
	 */
	protected JLabel numAgentsLabel;
	/**
	 * The maximum agent sight value allowed
	 */
	public static final int MAX_AGENT_SIGHT = 100;
	/**
	 * The minimum agent sight value allowed
	 */
	public static final int MIN_AGENT_SIGHT = 5;
	/**
	 * The agent sight value
	 */
	protected int agentSight = 60;
	/**
	 * The "Agent Sight" slider
	 */
	protected JSlider agentSightSlider;
	/**
	 * The "Agent Sight" label
	 */
	protected JLabel agentSightLabel;
	/**
	 * The maximum number of neighbors to follow allowed
	 */
	public static final int MAX_NUM_NEIGHBORS_TO_FOLLOW = 10;
	/**
	 * The minimum number of neighbors to follow allowed
	 */
	public static final int MIN_NUM_NEIGHBORS_TO_FOLLOW = 1;
	/**
	 * The number of neighbors to follow
	 */
	protected int numNeighborsToFollow = 3;
	/**
	 * The "Number of Neighbors to Follow" slider
	 */
	protected JSlider numNeighborsToFollowSlider;
	/**
	 * The "Number of Neighbors to Follow" label
	 */
	protected JLabel numNeighborsToFollowLabel;
	/**
	 * The maximum randomization factor allowed
	 */
	public static final int MAX_RANDOMIZATION_FACTOR = 100;
	/**
	 * The minimum randomization factor allowed
	 */
	public static final int MIN_RANDOMIZATION_FACTOR = 0;
	/**
	 * The randomization factor
	 */
	protected int randomizationFactor = 10;
	/**
	 * The "Randomization Factor" slider
	 */
	protected JSlider randomizationFactorSlider;
	/**
	 * The "Randomization Factor" label
	 */
	protected JLabel randomizationFactorLabel;
	/**
	 * The "Choose Neighbors by Proximity" checkbox
	 */
	protected JCheckBox chooseNeighborsByProximityCheckBox;
	/**
	 * How to choose neighbors to follow
	 */
	protected boolean chooseNeighborsByProximity = true;

	private static final NumberFormat PERCENT_FORMAT = NumberFormat.getPercentInstance();

	/**
	 * Initializes the world with the appropriate number of SwarmAgents
	 * with the appropriate "agent sight" settings
	 *
	 * @return A world with SwarmAgents
	 */
	protected World initializeWorld()
	{
		final World world = new World(300, 300);
		try
		{
			for (int i = 0; i < numAgents; i++)
				world.addObject(new SwarmAgent(5, agentSight, 5, 3, numNeighborsToFollow, chooseNeighborsByProximity, randomizationFactor / 100.0));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return world;
	}

	/**
	 * Returns a settings panel with "Number of Agents" and "Agent Sight"
	 * sliders
	 *
	 * @return A settings panel with "Number of Agents" and "Agent Sight"
	 *         sliders
	 */
	protected JPanel getSettingsPanel()
	{
		final JPanel settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));

		numAgentsLabel = new JLabel("Number of Agents: " + numAgents);
		numAgentsLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numAgentsLabel);

		final Hashtable numAgentsLabels = new Hashtable();
		numAgentsLabels.put(new Integer(MIN_NUM_AGENTS), new JLabel(Integer.toString(MIN_NUM_AGENTS)));
		numAgentsLabels.put(new Integer(MAX_NUM_AGENTS), new JLabel(Integer.toString(MAX_NUM_AGENTS)));

		numAgentsSlider = new JSlider(MIN_NUM_AGENTS, MAX_NUM_AGENTS, numAgents);
		numAgentsSlider.setLabelTable(numAgentsLabels);
		numAgentsSlider.setPaintLabels(true);
		numAgentsSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numAgentsChanged(event);
			}
		});
		settingsPanel.add(numAgentsSlider);

		agentSightLabel = new JLabel("Agent Sight: " + agentSight);
		agentSightLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(agentSightLabel);

		final Hashtable agentSightLabels = new Hashtable();
		agentSightLabels.put(new Integer(MIN_AGENT_SIGHT), new JLabel(Integer.toString(MIN_AGENT_SIGHT)));
		agentSightLabels.put(new Integer(MAX_AGENT_SIGHT), new JLabel(Integer.toString(MAX_AGENT_SIGHT)));

		agentSightSlider = new JSlider(MIN_AGENT_SIGHT, MAX_AGENT_SIGHT, agentSight);
		agentSightSlider.setLabelTable(agentSightLabels);
		agentSightSlider.setPaintLabels(true);
		agentSightSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				agentSightChanged(event);
			}
		});
		settingsPanel.add(agentSightSlider);

		numNeighborsToFollowLabel = new JLabel("Number of Neighbors to Follow: " + numNeighborsToFollow);
		numNeighborsToFollowLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(numNeighborsToFollowLabel);

		final Hashtable numNeighborsToFollowLabels = new Hashtable();
		numNeighborsToFollowLabels.put(new Integer(MIN_NUM_NEIGHBORS_TO_FOLLOW), new JLabel(Integer.toString(MIN_NUM_NEIGHBORS_TO_FOLLOW)));
		numNeighborsToFollowLabels.put(new Integer(MAX_NUM_NEIGHBORS_TO_FOLLOW), new JLabel(Integer.toString(MAX_NUM_NEIGHBORS_TO_FOLLOW)));

		numNeighborsToFollowSlider = new JSlider(MIN_NUM_NEIGHBORS_TO_FOLLOW, MAX_NUM_NEIGHBORS_TO_FOLLOW, numNeighborsToFollow);
		numNeighborsToFollowSlider.setLabelTable(numNeighborsToFollowLabels);
		numNeighborsToFollowSlider.setPaintLabels(true);
		numNeighborsToFollowSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				numNeighborsToFollowChanged(event);
			}
		});
		settingsPanel.add(numNeighborsToFollowSlider);

		randomizationFactorLabel = new JLabel("Randomization Factor: " + PERCENT_FORMAT.format(randomizationFactor / 100.0));
		randomizationFactorLabel.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(randomizationFactorLabel);

		final Hashtable randomizationFactorLabels = new Hashtable();
		randomizationFactorLabels.put(new Integer(MIN_RANDOMIZATION_FACTOR), new JLabel(PERCENT_FORMAT.format(MIN_RANDOMIZATION_FACTOR / 100.0)));
		randomizationFactorLabels.put(new Integer(MAX_RANDOMIZATION_FACTOR), new JLabel(PERCENT_FORMAT.format(MAX_RANDOMIZATION_FACTOR / 100.0)));

		randomizationFactorSlider = new JSlider(MIN_RANDOMIZATION_FACTOR, MAX_RANDOMIZATION_FACTOR, randomizationFactor);
		randomizationFactorSlider.setLabelTable(randomizationFactorLabels);
		randomizationFactorSlider.setPaintLabels(true);
		randomizationFactorSlider.addChangeListener(new ChangeListener()
		{
			public void stateChanged(final ChangeEvent event)
			{
				randomizationFactorChanged(event);
			}
		});
		settingsPanel.add(randomizationFactorSlider);

		chooseNeighborsByProximityCheckBox = new JCheckBox("Choose Neighbors by Proximity");
		chooseNeighborsByProximityCheckBox.setSelected(chooseNeighborsByProximity);
		chooseNeighborsByProximityCheckBox.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		chooseNeighborsByProximityCheckBox.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				chooseNeighborsByProximityClicked(event);
			}
		});
		settingsPanel.add(chooseNeighborsByProximityCheckBox);

		final JLabel changes = new JLabel("Changes will take");
		changes.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		final JLabel takeEffect = new JLabel("effect at next reset");
		takeEffect.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT);
		settingsPanel.add(changes);
		settingsPanel.add(takeEffect);
		return settingsPanel;
	}

	/**
	 * The method invoked when the "Number of Agents" slider changes
	 *
	 * @param event The change event
	 */
	public void numAgentsChanged(final ChangeEvent event)
	{
		numAgents = numAgentsSlider.getValue();
		numAgentsLabel.setText("Number of Agents: " + numAgents);
	}

	/**
	 * The method invoked when the "Agent Sight" slider changes
	 *
	 * @param event The change event
	 */
	public void agentSightChanged(final ChangeEvent event)
	{
		agentSight = agentSightSlider.getValue();
		agentSightLabel.setText("Agent Sight: " + agentSight);
	}

	/**
	 * The method invoked when the "Number of Neighbors to Follow" slider changes
	 *
	 * @param event The change event
	 */
	public void numNeighborsToFollowChanged(final ChangeEvent event)
	{
		numNeighborsToFollow = numNeighborsToFollowSlider.getValue();
		numNeighborsToFollowLabel.setText("Number of Neighbors to Follow: " + numNeighborsToFollow);
	}

	/**
	 * The method invoked when the "Randomization Factor" slider changes
	 *
	 * @param event The change event
	 */
	public void randomizationFactorChanged(final ChangeEvent event)
	{
		randomizationFactor = randomizationFactorSlider.getValue();
		randomizationFactorLabel.setText("Randomization Factor: " + PERCENT_FORMAT.format(randomizationFactor / 100.0));
	}

	/**
	 * The method invoked when the user selects or deselects the "Choose Neighbors
	 * by Proximity" check box
	 *
	 * @param event The action event
	 */
	public void chooseNeighborsByProximityClicked(final ActionEvent event)
	{
		chooseNeighborsByProximity = chooseNeighborsByProximityCheckBox.isSelected();
	}
}