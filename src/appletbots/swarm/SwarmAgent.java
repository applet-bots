/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots.swarm;

import appletbots.Agent;
import appletbots.geometry.Vector;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * A swarm agent looks at its neighbors, chooses a certain number
 * of them, and accelerates in the direction of their average velocity,
 * with a part of its acceleration being randomized.
 *
 * @author Erik Rasmussen
 */
public class SwarmAgent extends Agent
{
	/**
	 * The number of neighbors to follow
	 */
	private int numNeighborsToFollow;

	/**
	 * Choose neighbors by proximity when true, randomly when false
	 */
	private boolean chooseNeighborsByProximity;

	/**
	 * Percentage of acceleration to be random
	 */
	private double randomizationFactor;

	/**
	 * Constructs a swarm agent with the given parameters
	 *
	 * @param size                       The agent's radius
	 * @param sight                      The distance the agent can see
	 * @param maxSpeed                   The maximum speed the agent can travel
	 * @param maxAcceleration            The maximum acceleration for this agent
	 * @param numNeighborsToFollow       The number of neightbors to follow
	 * @param chooseNeighborsByProximity How to choose neighbors to follow
	 * @param randomizationFactor        The percentage of acceleration to be random
	 */
	public SwarmAgent(final int size, final int sight, final double maxSpeed, final double maxAcceleration, final int numNeighborsToFollow, final boolean chooseNeighborsByProximity, final double randomizationFactor)
	{
		super(size, sight, maxSpeed, maxAcceleration);
		this.numNeighborsToFollow = numNeighborsToFollow;
		this.chooseNeighborsByProximity = chooseNeighborsByProximity;
		this.randomizationFactor = randomizationFactor;
		this.color = Color.cyan;
	}

	/**
	 * Observes the world, and follows the Swarm Agent Algorithm.
	 */
	public void observeWorld()
	{
		final List neighbors = world.getNeighbors(this);
		if (chooseNeighborsByProximity)
			Collections.sort(neighbors, world.objectDistanceComparator(this));
		else
			Collections.shuffle(neighbors);

		final List velocitiesOfNeighborsToFollow = new ArrayList();
		for (int i = 0; i < neighbors.size() && i < numNeighborsToFollow; i++)
		{
			final Agent neighbor = (Agent) neighbors.get(i);
			velocitiesOfNeighborsToFollow.add(world.getVelocity(neighbor));
		}

		final Vector randomization = Vector.getRandom(getMaxAcceleration() * randomizationFactor);
		final Vector averageNeighborVelocity = Vector.average(velocitiesOfNeighborsToFollow).multiply(1 - randomizationFactor);

		setAcceleration(averageNeighborVelocity.add(randomization));
	}
}