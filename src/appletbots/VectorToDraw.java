/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

import appletbots.geometry.Vector;

import java.awt.*;

/**
 * This class represents a vector to be drawn when painting the world.  This
 * is used when showing the velocities or accelerations of agents.
 *
 * @author Erik Rasmussen
 */
public class VectorToDraw
{
	/**
	 * The vector to draw
	 */
	public Vector vector;
	/**
	 * The color with which to draw the vector
	 */
	public Color color;

	/**
	 * Creates a new vector-to-draw object with the given parameters
	 *
	 * @param vector The vector to draw
	 * @param color  The color with which to draw the vector
	 */
	public VectorToDraw(final Vector vector, final Color color)
	{
		this.vector = vector;
		this.color = color;
	}
}