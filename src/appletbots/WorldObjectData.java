/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

import appletbots.geometry.Point;
import appletbots.geometry.Vector;

/**
 * This object contains the location and velocity values for an object in the
 * world
 *
 * @author Erik Rasmussen
 */
public class WorldObjectData
{
	/**
	 * The object's location
	 */
	private Point location;
	/**
	 * The object's velocity
	 */
	private Vector velocity;
	/**
	 * The object
	 */
	private WorldObject object;

	/**
	 * Constructs a new WorldObjectData with the given location and velocity
	 * values for the given object
	 *
	 * @param location The location of the object
	 * @param velocity The velocity of the object
	 * @param object   The object
	 */
	public WorldObjectData(final Point location, final Vector velocity, final WorldObject object)
	{
		this.location = location;
		this.velocity = velocity;
		this.object = object;
	}

	/**
	 * Returns the location of the object.  Note that the point returned is a
	 * clone of the internal location point, so changing the returned point
	 * will not affect the object's location.  To modify the object's location,
	 * use setLocation(Point).
	 *
	 * @return The location of the object
	 */
	public Point getLocation()
	{
		return (Point) location.clone();
	}

	/**
	 * Sets the location of the object
	 *
	 * @param location Sets the location of the object
	 */
	public void setLocation(final Point location)
	{
		this.location = (Point) location.clone();
	}

	/**
	 * Returns the velocity of the object.  Note that the vector returned is a
	 * clone of the internal velocity vector, so changing the returned vector
	 * will not affect the object's velocity.  To modify the object's velocity,
	 * use setVelocity(Vector).
	 *
	 * @return The velocity of the object
	 */
	public Vector getVelocity()
	{
		return (Vector) velocity.clone();
	}

	/**
	 * Sets the velocity of the object
	 *
	 * @param velocity The velocity of the object
	 */
	public void setVelocity(final Vector velocity)
	{
		this.velocity = (Vector) velocity.clone();
		if (this.velocity.getLength() > object.getMaxSpeed())
			this.velocity = this.velocity.setLength(object.getMaxSpeed());
	}

	/**
	 * Returns the object
	 *
	 * @return The object
	 */
	public WorldObject getObject()
	{
		return object;
	}
}