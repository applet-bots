/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;


/**
 * This is an agent that doesn't really react to stimuli in the world, it
 * simply accelerates in a random direction.  If you're trying to write an
 * agent and a RandomAgent performs the task better than your agent, you
 * <b>seriously</b> need to take another look at your agent algorithm!
 *
 * @author Erik Rasmussen
 */
public class RandomAgent extends Agent
{
  /**
   * Ignores any world stimuli and sets the acceleration randomly.
   */
  public void observeWorld()
  {
    setAcceleration(getRandomAcceleration());
  }
}