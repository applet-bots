/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserved
 */
package appletbots;

import java.awt.*;

/**
 * This class represents an object that can be picked up in the appletbots
 * world.
 *
 * @author Erik Rasmussen
 */
public abstract class CarriableObject extends WorldObject
{
	/**
	 * The agent carrying the object
	 */
	private CarrierAgent carriedBy;

	/**
	 * Creates a new CarriableObject with the given parameters
	 *
	 * @param mass     The object's mass
	 * @param maxSpeed The object's maximum speed
	 * @param size     The object's radius
	 * @param color    The object's color
	 */
	public CarriableObject(final double mass, final double maxSpeed, final int size, final Color color)
	{
		super(mass, maxSpeed, size, color);
	}

	/**
	 * Returns whether or not the object is being carried
	 *
	 * @return Whether or not the object is being carried
	 */
	public boolean isCarried()
	{
		return carriedBy != null;
	}

	/**
	 * Informs the object about who is carrying it
	 *
	 * @param carriedBy The agent carrying the object
	 */
	public void setCarriedBy(final CarrierAgent carriedBy)
	{
		this.carriedBy = carriedBy;
	}

	/**
	 * Returns the agent carrying the object
	 *
	 * @return The agent carrying the object
	 */
	public CarrierAgent getCarriedBy()
	{
		return carriedBy;
	}
}