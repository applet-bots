/*
 * Copyright (c) 2002 Erik Rasmussen - All Rights Reserverd
 */
package appletbots;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class represents an object viewer panel to be used with the appletbots
 * applet.  It becomes active when an object in the world is selected.
 *
 * @author Erik Rasmussen
 */
public class ObjectViewer extends JPanel
{
	/**
	 * The location and velocity data of the selected object
	 */
	protected WorldObjectData data;

	/**
	 * The "Show Sight" check box
	 */
	protected JCheckBox showSightCheckBox;
	/**
	 * The "Show Velocity" check box
	 */
	protected JCheckBox showVelocityCheckBox;
	/**
	 * The "Show Acceleration" check box
	 */
	protected JCheckBox showAccelerationCheckBox;

	/**
	 * Creates a new object viewer panel
	 */
	public ObjectViewer()
	{
		showSightCheckBox = new JCheckBox();
		showSightCheckBox.setEnabled(false);
		showSightCheckBox.setSelected(false);

		showVelocityCheckBox = new JCheckBox();
		showVelocityCheckBox.setEnabled(false);
		showVelocityCheckBox.setSelected(false);
		showVelocityCheckBox.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				ObjectViewer.this.showVelocityClicked(event);
			}
		});

		showAccelerationCheckBox = new JCheckBox();
		showAccelerationCheckBox.setEnabled(false);
		showAccelerationCheckBox.setSelected(false);
		showAccelerationCheckBox.addActionListener(new ActionListener()
		{
			public void actionPerformed(final ActionEvent event)
			{
				ObjectViewer.this.showAccelerationClicked(event);
			}
		});

		setLayout(new java.awt.GridLayout(3, 2));
		add(new JLabel("Show Sight"));
		add(showSightCheckBox);
		add(new JLabel("Show Velocity"));
		add(showVelocityCheckBox);
		add(new JLabel("Show Acceleration"));
		add(showAccelerationCheckBox);
	}

	/**
	 * Sets a new selected object by passing the object's location and
	 * velocity data
	 *
	 * @param data The selected object's location and velocity data
	 */
	public void setData(final WorldObjectData data)
	{
		this.data = data;
		if (data != null && data.getObject() instanceof Agent)
		{
			final Agent agent = (Agent) data.getObject();
			showSightCheckBox.setEnabled(true);
			showVelocityCheckBox.setEnabled(true);
			showVelocityCheckBox.setSelected(agent.getShowVelocity());
			showAccelerationCheckBox.setEnabled(true);
			showAccelerationCheckBox.setSelected(agent.getShowAcceleration());
		}
		else
		{
			showSightCheckBox.setSelected(false);
			showSightCheckBox.setEnabled(false);
			showVelocityCheckBox.setSelected(false);
			showVelocityCheckBox.setEnabled(false);
			showAccelerationCheckBox.setSelected(false);
			showAccelerationCheckBox.setEnabled(false);
		}
		update();
	}

	/**
	 * Updates the panel
	 */
	public void update()
	{
	}

	/**
	 * Returns whether or not the "Show Sight" check box is checked
	 *
	 * @return Whether or not the "Show Sight" check box is checked
	 */
	public boolean getShowSight()
	{
		return showSightCheckBox.isSelected();
	}

	/**
	 * The method invoked when the user selects or deselects the "Show
	 * Velocity" check box
	 *
	 * @param event The action event
	 */
	public void showVelocityClicked(final ActionEvent event)
	{
		if (data != null && data.getObject() instanceof Agent)
			((Agent) data.getObject()).setShowVelocity(showVelocityCheckBox.isSelected());
	}

	/**
	 * The method invoked when the user selects or deselects the "Show
	 * Acceleration" check box
	 *
	 * @param event The action event
	 */
	public void showAccelerationClicked(final ActionEvent event)
	{
		if (data != null && data.getObject() instanceof Agent)
			((Agent) data.getObject()).setShowAcceleration(showAccelerationCheckBox.isSelected());
	}
}